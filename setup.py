from setuptools import setup

import sys, os, io
with io.open(os.path.join(os.path.dirname(sys.argv[0]), 'banhost', 'version.py')) as infh:
    exec(infh.read())

setup(
    name = "banhost",
    packages = ["banhost"],
    test_suite="tests",
    entry_points = {
        "console_scripts": ['banhost = banhost:main']
        },
    version = __version__,
    description = "Ban hosts using netfilter commands.",
    long_description = "Banr hosts using netfilter commands.",
    author = "Laurent Pelecq",
    author_email = "lpelecq+banhost@circoise.eu",
    license='GNU General Public License version 3'
    )
