# Copyright (C) 2018 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import unittest

import banhost
from banhost.sql import *

class TestStatement(unittest.TestCase):

    def test_string_representation(self):
        """Statement operator str"""
        sql = 'SELECT * FROM table WHERE x > ?'
        stmt = banhost.sql.Statement(sql, (3,))
        self.assertEqual('{} with (3,)'.format(sql), str(stmt))

class TestTable(unittest.TestCase):

    COLUMNS = [('id', 'integer'), ('name', 'text'), ('age', 'integer')]

    def test_schema(self):
        """Table schemas"""
        table1 = Table('persons', self.COLUMNS)
        schema1 = ("CREATE TABLE IF NOT EXISTS persons (id integer, name text, age integer)",)
        self.assertEqual(schema1, table1.schema())
        table2 = Table('persons', self.COLUMNS, primary='id')
        schema2 = ("CREATE TABLE IF NOT EXISTS persons (id integer PRIMARY KEY, name text, age integer)",)
        self.assertEqual(schema2, table2.schema())
        table3 = Table('persons', self.COLUMNS, primary='id', unique=['name'])
        schema3 = ("CREATE TABLE IF NOT EXISTS persons (id integer PRIMARY KEY, name text UNIQUE, age integer)",)
        self.assertEqual(schema3, table3.schema())

class TestStatementBuilder(unittest.TestCase):

    """Test class StatementBuilder."""

    class TablePersons(Table):
        COLUMNS = [('id', 'integer'), ('firstname', 'text'), ('lastname', 'text'), ('age', 'integer'), ('city', 'integer')]
        def __init__(self):
            super().__init__('persons', self.COLUMNS, 'id')

    class TablePlaces(Table):
        COLUMNS = [('id', 'integer'), ('name', 'text'), ('kind', 'text'), ('city', 'integer')]
        def __init__(self):
            super().__init__('places', self.COLUMNS, 'id')

    class TableCities(Table):
        COLUMNS = [('id', 'integer'), ('name', 'text'), ('size', 'integer')]
        def __init__(self):
            super().__init__('cities', self.COLUMNS, 'id')

    def test_quoteValue(self):
        """Test method quoteValue"""
        sb = StatementBuilder().select('firstname').fromTable(self.TablePersons())
        Litteral = banhost.sql.Litteral
        self.assertEqual(Litteral(10), sb.quoteValue(10))
        self.assertEqual(Litteral(10), sb.quoteValue(Litteral(10)))
        self.assertEqual(Litteral('someone'), sb.quoteValue('someone'))
        self.assertEqual(Litteral('someone'), sb.quoteValue(Litteral('someone')))
        self.assertEqual('firstname', sb.quoteValue('firstname'))

    def test_incompatibilities(self):
        """Test errors for incompatibles actions"""
        table = self.TablePersons()
        with self.assertRaises(SQLError):
            StatementBuilder().get()
        # Select statement
        sb1 = StatementBuilder().select('name')
        with self.assertRaises(SQLError):
            sb1.insertInto(table)
        with self.assertRaises(SQLError):
            sb1.delete()
        with self.assertRaises(SQLError):
            sb1.count()
        with self.assertRaises(SQLError):
            sb1.inColumns('name')
        with self.assertRaises(SQLError):
            sb1.values('someone', 10)
        sb1.fromTable(table) # Test no exception raised
        with self.assertRaises(SQLError):
            sb1.fromTable(table).where('nok')
        # Insert statement
        sb2 = StatementBuilder().insertInto(table)
        with self.assertRaises(SQLError):
            sb2.select('name', 'age')
        with self.assertRaises(SQLError):
            sb2.inColumns('name', 'age').inColumns('name')
        with self.assertRaises(SQLError):
            sb2.where(EqualTo('name', 'someone'))

    def test_no_action(self):
        """Test errors when action is not set"""
        table = self.TablePersons()
        with self.assertRaises(SQLError):
            StatementBuilder().fromTable(table)
        with self.assertRaises(SQLError):
            StatementBuilder().inColumns('name', 'age')
        with self.assertRaises(SQLError):
            StatementBuilder().where(EqualTo('name', 'someone')).get()

    def test_ambiguous_column(self):
        """Query an ambiguous column"""
        query = StatementBuilder().select('name', 'age').fromTable(self.TablePlaces()).fromTable(self.TableCities())
        with self.assertRaises(SQLError):
            query.get()

    def test_unknown_column(self):
        """Query an unknown column"""
        query = StatementBuilder().select('unknown', 'age').fromTable(self.TablePersons())
        with self.assertRaises(SQLError):
            query.get()

    def test_single_table(self):
        """Query with a single table and no where clause"""
        query = StatementBuilder().select('firstname', 'lastname', 'age').fromTable(self.TablePersons()).get()
        sql = 'SELECT firstname, lastname, age FROM persons'
        self.assertEqual(sql, query.sql)
        self.assertEqual([], query.parameters)
        self.assertEqual((sql,), query())

    def test_multi_tables(self):
        """Query with two tables and no where clause"""
        query = StatementBuilder().select('p.lastname', 'p.age').fromTable(self.TablePersons(), 'p').fromTable(self.TableCities()).get()
        sql = 'SELECT p.lastname, p.age FROM persons AS p, cities'
        self.assertEqual(sql, query.sql)
        self.assertEqual([], query.parameters)
        self.assertEqual((sql,), query())

    def test_single_table_where(self):
        """Query with a single table and a where clause"""
        query = (StatementBuilder()
                 .select('firstname', 'lastname', 'age')
                 .fromTable(self.TablePersons())
                 .where(Or(Like('firstname', 'a%'),
                           EqualTo('lastname', 'Butterfly'),
                           And(GreaterThan('age', Integer('kid')),
                               LessThanOrEqualTo('age', 60))))
                 .get())
        sql = 'SELECT firstname, lastname, age FROM persons WHERE firstname LIKE ? OR lastname = ? OR (age > ? AND age <= ?)'
        self.assertEqual(sql, query.sql)
        self.assertEqual((sql, ('a%', 'Butterfly', 17, 60)), query(kid=17))

    def test_single_table_order(self):
        """Query with a single table and an order-by"""
        base_sql = 'SELECT firstname, lastname, age FROM persons WHERE age >= ?'
        sql1 = base_sql + ' ORDER BY lastname'
        # undefined
        query1 = (StatementBuilder()
                  .select('firstname', 'lastname', 'age')
                  .fromTable(self.TablePersons())
                  .where(GreaterThanOrEqualTo('age', 18))
                  .orderBy('lastname')
                  .get())
        self.assertEqual((sql1, (18,)), query1())
        # ascendent and descendent
        sql2 = base_sql + " ORDER BY firstname ASC, lastname ASC, age DESC"
        query2 = (StatementBuilder()
                  .select('firstname', 'lastname', 'age')
                  .fromTable(self.TablePersons())
                  .where(GreaterThanOrEqualTo('age', 18))
                  .ascendentOrderBy('firstname')
                  .ascendentOrderBy('lastname')
                  .decendentOrderBy('age')
                  .get())
        self.assertEqual((sql2, (18,)), query2())

    def test_single_table_in_list(self):
        """Query with a single table and a where clause with operator IN"""
        with self.assertRaises(SQLError):
            IsIn('age', 'invalid')

        sql = 'SELECT lastname, age FROM persons WHERE age IN (?, ?, ?)'
        ages = [12, 18, 24]
        query_no_var = (StatementBuilder()
                        .select('lastname', 'age')
                        .fromTable(self.TablePersons())
                        .where(IsIn('age', ages))
                        .get())
        self.assertEqual((sql, (12, 18, 24)), query_no_var())

        query_var = (StatementBuilder()
                     .select('lastname', 'age')
                     .fromTable(self.TablePersons())
                     .where(IsIn('age', List('ages')))
                     .get())
        self.assertEqual((sql, (12, 18, 24)), query_var(ages=ages))

    def test_count_single_table(self):
        """Count rows in a single table"""
        sql = 'SELECT COUNT(lastname) FROM persons WHERE age > ?'
        query = (StatementBuilder()
                 .count('lastname')
                 .fromTable(self.TablePersons())
                 .where(GreaterThan('age', Integer('age')))
                 .get())
        self.assertEqual((sql, (17,)), query(age=17))

    def test_count_multi_table(self):
        """Count rows with a join"""
        sql = 'SELECT COUNT(*) FROM persons AS p, cities AS c WHERE p.city = c.id AND c.size > ?'
        query = (StatementBuilder()
                 .count()
                 .fromTable(self.TablePersons(), 'p')
                 .fromTable(self.TableCities(), 'c')
                 .where(And(EqualTo('p.city', 'c.id'), GreaterThan('c.size', Integer('size'))))
                 .get())
        self.assertEqual((sql, (20000,)), query(size=20000))

    def test_delete_where(self):
        """Delete query with a single table and a where clause"""
        query = (StatementBuilder()
                 .delete()
                 .fromTable(self.TablePersons())
                 .where(GreaterThan('age', Integer('age')))
                 .get())
        sql = 'DELETE FROM persons WHERE age > ?'
        self.assertEqual((sql, (16,)), query(age=16))

    def test_insert_all(self):
        """Insert query with all columns"""
        stmt = (StatementBuilder()
                .insertInto(self.TableCities())
                .values('acapulco', 123)
                .get())
        sql = 'INSERT INTO cities VALUES (NULL, ?, ?)'
        self.assertEqual((sql, ('acapulco', 123)), stmt())

    def test_insert_some(self):
        """Insert query with all columns"""
        stmt = (StatementBuilder()
                .insertInto(self.TableCities())
                .inColumns('name', 'size')
                .values('acapulco', 123)
                .get())
        sql = 'INSERT INTO cities (name, size) VALUES (?, ?)'
        self.assertEqual((sql, ('acapulco', 123)), stmt())

    def test_table_select_join(self):
        """Select query with join"""
        query = (StatementBuilder()
                 .select('firstname', 'lastname', 'age')
                 .fromTable(self.TablePersons())
                 .fromTable(self.TableCities())
                 .where(And(EqualTo('city', 'id'),
                            LessThan('size', Integer('size'))))
                 .get())
        sql = 'SELECT firstname, lastname, age FROM persons, cities WHERE city = id AND size < ?'
        self.assertEqual(sql, query.sql)
        self.assertEqual((sql, (1000,)), query(size=1000))

        query = (StatementBuilder()
                 .select('p.name', 'kind')
                 .fromTable(self.TablePlaces(), 'p')
                 .fromTable(self.TableCities(), 'c')
                 .where(And(EqualTo('p.city', 'c.id'),
                            LessThan('c.size', Integer('size'))))
                 .get())
        sql = 'SELECT p.name, kind FROM places AS p, cities AS c WHERE p.city = c.id AND c.size < ?'
        self.assertEqual(sql, query.sql)
        self.assertEqual((sql, (1000,)), query(size=1000))

    def test_nested_statements(self):
        """Nested statements."""
        inner_query = (StatementBuilder()
                       .select('id')
                       .fromTable(self.TableCities())
                       .where(LessThan('size', Integer('size')))
                       .get())
        outter_query = (StatementBuilder()
                        .delete()
                        .fromTable(self.TablePersons())
                        .where(IsNotIn('city', inner_query))
                        .get())
        sql = "DELETE FROM persons WHERE city NOT IN (SELECT id FROM cities WHERE size < ?)"
        self.assertEqual((sql, (1000,)), outter_query(size=1000))

    def test_union_statements(self):
        """Nested statements."""
        inner_query1 = (StatementBuilder()
                       .select('id')
                       .fromTable(self.TableCities())
                       .where(LessThan('size', Integer('min')))
                       .get())
        inner_query2 = (StatementBuilder()
                       .select('id')
                       .fromTable(self.TableCities())
                       .where(GreaterThan('size', Integer('max')))
                       .get())
        outter_query = (StatementBuilder()
                        .delete()
                        .fromTable(self.TablePersons())
                        .where(IsIn('city', Union(inner_query1, inner_query2)))
                        .get())
        sql = "DELETE FROM persons WHERE city IN (SELECT id FROM cities WHERE size < ? UNION SELECT id FROM cities WHERE size > ?)"
        self.assertEqual((sql, (1000, 10000)), outter_query(min=1000, max=10000))

    def test_unary_operators(self):
        """Unary operators."""
        query1 = (StatementBuilder()
                  .select('id')
                  .fromTable(self.TableCities())
                  .where(Or(IsNull('name'), EqualTo('name', '')))
                  .get())
        sql = 'SELECT id FROM cities WHERE name IS NULL OR name = ?'
        self.assertEqual((sql, ('',)), query1())

        query2 = (StatementBuilder()
                  .select('id', 'name')
                  .fromTable(self.TableCities())
                  .where(And(IsNotNull('name'), IsNull('size')))
                  .get())
        sql = 'SELECT id, name FROM cities WHERE name IS NOT NULL AND size IS NULL'
        self.assertEqual((sql,), query2())

    def test_update(self):
        """Update row."""
        # with quoted values
        stmt = (StatementBuilder()
                .update(self.TableCities())
                .setColumn('name', 'acapulco')
                .setColumn('size', Integer('size'))
                .where(EqualTo('id', Integer('id')))
                .get())
        sql = 'UPDATE cities SET name = ?, size = ? WHERE id = ?'
        self.assertEqual((sql, ('acapulco', 10000, 3)), stmt(size=10000, id=3))

        # with unquoted values
        stmt = (StatementBuilder()
                .update(self.TablePersons())
                .setColumn('firstname', 'lastname')
                .where(EqualTo('id', Integer('id')))
                .get())
        sql = 'UPDATE persons SET firstname = lastname WHERE id = ?'
        self.assertEqual((sql, (3,)), stmt(id=3))

if __name__ == '__main__':
    unittest.main()
