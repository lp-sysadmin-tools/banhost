# Copyright (C) 2005-2018 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import banhost

import contextlib
import io
import json
import os
import shutil
import sys
import tempfile
import unittest

from unittest.mock import call, patch

from configparser import ConfigParser
from collections import namedtuple

from tests.helpers.asserts import *
from tests.helpers.data import *
from tests.helpers.dns import *

chain_in = "in_banhost"
target_in = "BLOCKLIST_INCOMING"
chain_out = "out_banhost"
target_out = "BLOCKLIST_OUTGOING"

quarantine = 2 * 3600
history_delay = 7 * 24 * 3600

try:
    Arguments = namedtuple('Args', ['compact', 'database', 'html_file', 'ip_addresses', 'json_file', 'list_hosts', 'log_level',
                                    'no_act', 'rebuild', 'remove', 'restore', 'stdout', 'update', 'upgrade'],
                           defaults=[False, None, None, [], None, False, None,
                                     False, False, False, False, False, False, False])
except TypeError:
    # prior to python 3.6
    class Arguments:
        def __init__(self, compact=False, database=None, html_file=None, ip_addresses=[], json_file=None, list_hosts=False,
                     log_level=None, no_act=False, rebuild=False, remove=False, restore=False, stdout=False, update=False,
                     upgrade=False):
            self.compact = compact
            self.database = database
            self.html_file = html_file
            self.ip_addresses = ip_addresses
            self.json_file = json_file
            self.list_hosts = list_hosts
            self.log_level = log_level
            self.no_act = no_act
            self.rebuild = rebuild
            self.remove = remove
            self.restore = restore
            self.stdout = stdout
            self.update = update
            self.upgrade = upgrade

def read_json(filename):
    """Read a json file."""
    with io.open(filename) as infh:
        return json.load(infh)

def make_config(working_dir, framework='nftables'):
    """Create a config parser instances."""
    config = ConfigParser()
    config.add_section('files')
    dbfile = os.path.join(working_dir, 'banhost.db')
    config.set('files', 'database', dbfile)
    with_nftables = False
    if framework == 'nftables':
        config.add_section('nftables')
        config.set('nftables', 'command', '/disable/nft')
        with_nftables = True
    elif framework == 'iptables':
        config.add_section('iptables')
        config.set('iptables', 'path4', '/disable/iptables')
        config.set('iptables', 'path6', '/disable/ip6tables')
    config.add_section('tables')
    if framework:
        config.set('tables', 'framework', framework)
    if with_nftables:
        config.set('tables', 'table', 'filter')
    config.set('tables', 'chain_in', 'in_banhost')
    config.set('tables', 'target_in', 'BLOCKLIST_INCOMING')
    config.set('tables', 'chain_out', 'out_banhost')
    config.set('tables', 'target_out', 'BLOCKLIST_OUTGOING')
    config.add_section('policy')
    config.set('policy', 'expire', str(quarantine))
    config.set('policy', 'trusted', '198.51.100.87 198.51.100.179')
    config.add_section('history')
    config.set('history', 'expire', str(history_delay))
    config.add_section('logging')
    logfile = os.path.join(working_dir, 'banhost.log')
    config.set('logging', 'output', logfile)
    config.set('logging', 'level', 'info')
    return config


LIST_IPV6_CHAIN_IN = """{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram", "json_schema_version": 1}}, {"chain": {"family": "inet", "table": "filter", "name": "in_banhost", "handle": 25}}, {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "handle": 33, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "saddr"}}, "right": "__IPV6__"}}, {"jump": {"target": "pr_in_banhost"}}]}}]}
""".replace('__IPV6__', TEST_IPV6_ADDRESS1)

LIST_IPV6_CHAIN_OUT = """{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram", "json_schema_version": 1}}, {"chain": {"family": "inet", "table": "filter", "name": "out_banhost", "handle": 26}}, {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "handle": 34, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "daddr"}}, "right": "__IPV6__"}}, {"jump": {"target": "pr_out_banhost"}}]}}]}""".replace('__IPV6__', TEST_IPV6_ADDRESS1)

def remove_last_rule(nft_output):
    """Remove the last rule of a nftables json output."""
    nft_object = json.loads(LIST_IPV6_CHAIN_IN)
    nft_object['nftables'].pop()
    return json.dumps(nft_object)

class TestRun(unittest.TestCase):

    """Test function run not depending on the framework."""

    def setUp(self):
        self.working_dir = tempfile.mkdtemp(prefix='tmp_banhost')

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    @patch('banhost.init_logging')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_config_no_database(self, mock_communicate, mock_init_logging):
        """Config without database."""
        config = make_config(self.working_dir)
        config.remove_option('files', 'database')
        status_file = os.path.join(self.working_dir, 'status.json')
        status_args = Arguments(False, None, None, [], status_file)
        with self.assertRaises(banhost.core.CoreException):
            with patch('logging.error'):
                banhost.run(status_args, config)
        assert_not_called(mock_communicate)

    @patch('banhost.init_logging')
    def test_config_no_framework(self, mock_init_logging):
        """Config no framework."""
        config = make_config(self.working_dir, None)
        status_args = Arguments(list_hosts=True)
        with self.assertRaises(banhost.core.CoreException):
            with patch('logging.error'):
                banhost.run(status_args, config)

    @patch('banhost.init_logging')
    def test_config_bad_framework(self, mock_init_logging):
        """Config bad framework."""
        config = make_config(self.working_dir, 'badfwk')
        status_args = Arguments(list_hosts=True)
        with self.assertRaises(banhost.core.CoreException):
            with patch('logging.error'):
                banhost.run(status_args, config)

    @patch('logging.info')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_action_rebuild(self, mock_communicate, mock_logging_info):
        """Check that action rebuild is called."""
        config = make_config(self.working_dir)
        args_rebuild = Arguments(rebuild=True)
        with patch('banhost.core.BlockList.rebuild') as mock_rebuild:
            banhost.run(args_rebuild, config)
            assert_called_once(mock_rebuild)
        assert_not_called(mock_communicate)

    @patch('logging.info')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_action_remove(self, mock_communicate, mock_logging_info):
        """Check that action remove is called."""
        config = make_config(self.working_dir)
        ip_addresses = [TEST_IPV6_ADDRESS1]
        args_remove = Arguments(ip_addresses=ip_addresses, remove=True)
        with patch('banhost.core.BlockList.remove') as mock_remove:
            banhost.run(args_remove, config)
            assert_called_once(mock_remove)
        assert_not_called(mock_communicate)

    @patch('logging.info')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_action_restore(self, mock_communicate, mock_logging_info):
        """Check that action restore is called."""
        config = make_config(self.working_dir)
        args_restore = Arguments(restore=True)
        with patch('banhost.core.BlockList.restore') as mock_restore:
            banhost.run(args_restore, config)
            assert_called_once(mock_restore)
        assert_not_called(mock_communicate)

    @patch('logging.info')
    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_action_update(self, mock_communicate, mock_gethostname, mock_logging_info):
        """Check that action update is called."""
        config = make_config(self.working_dir)
        args_update = Arguments(update=True)
        with patch('banhost.core.BlockList.update') as mock_update:
            banhost.run(args_update, config)
            assert_called_once(mock_update)
        assert_not_called(mock_communicate)

    @patch('logging.info')
    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_action_upgrade(self, mock_communicate, mock_gethostname, mock_logging_info):
        """Check that action upgrade is called."""
        config = make_config(self.working_dir)
        with banhost.sqlite.Connection(config.get('files', 'database')) as connection:
            with connection.transaction() as transaction:
                transaction.cursor.executescript(CONTENT_V4)
        upgrade_args = Arguments(upgrade=True)
        banhost.run(upgrade_args, config) # upgrade
        banhost.run(upgrade_args, config) # up to date
        mock_logging_info.assert_has_calls([call("database schema has been upgraded"),
                                            call("database schema is up to date")])
        assert_not_called(mock_communicate)

    @patch('logging.info')
    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    @patch('banhost.core.BlockList.generate_html')
    def test_status_html(self, mock_genhtml, mock_communicate, mock_gethostname, mock_logging_info):
        """Check that status html is called."""
        config = make_config(self.working_dir)
        html_file = "/fake/status.html"
        status_args = Arguments(html_file=html_file)
        banhost.run(status_args, config)
        mock_genhtml.assert_called_once_with(html_file)
        assert_not_called(mock_communicate)

    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.init_logging')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_compact_database(self, mock_communicate, mock_init_logging, mock_gethostname):
        """Compact database on exit."""
        config = make_config(self.working_dir)

        no_compact_args = Arguments()
        with patch('banhost.sqlite.Database.vacuum') as mock_vacuum:
            banhost.run(no_compact_args, config)
            assert_not_called(mock_vacuum)

        compact_args = Arguments(compact=True)
        with patch('banhost.sqlite.Database.vacuum') as mock_vacuum:
            banhost.run(compact_args, config)
            assert_called_once(mock_vacuum)
        assert_not_called(mock_communicate)

    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_init_logging(self, mock_communicate, mock_gethostname):
        """Initialize logging."""
        import logging
        levels = { 'debug': logging.DEBUG, 'verbose': logging.INFO, 'quiet': logging.ERROR }
        for level_name in levels:
            properties = {}
            with patch("logging.Logger.setLevel") as mock_set_logger:
                with patch("logging.Logger.addHandler") as mock_add_handler:
                    banhost.init_logging(level_name)
                    self.assertIsInstance(mock_add_handler.call_args[0][0], logging.StreamHandler)
                mock_set_logger.assert_called_once_with(levels[level_name])

        with patch("logging.Logger.setLevel"):
            with patch("logging.Logger.addHandler") as mock_add_handler:
                banhost.init_logging(output='stream')
                self.assertIsInstance(mock_add_handler.call_args[0][0], logging.StreamHandler)

        with patch("logging.Logger.setLevel"):
            with patch("logging.Logger.addHandler") as mock_add_handler:
                with self.assertRaises(Exception):
                    banhost.init_logging(output='syslog', facility='not_a_facility')
                with patch("socket.socket") as mock_socket:
                    banhost.init_logging(output='syslog', facility='daemon')
                    assert_called_once(mock_socket)
                assert_called_once(mock_add_handler)

        with patch("logging.Logger.setLevel"):
            with patch("logging.Logger.addHandler") as mock_add_handler:
                with patch("logging.handlers.RotatingFileHandler") as mock_handler:
                    banhost.init_logging(output=os.path.join(self.working_dir, 'unittest.log'))
                    assert_called_once(mock_handler)
                assert_called_once(mock_add_handler)

        # Config with an invalid logging option
        config = make_config(self.working_dir)
        config.set('logging', 'invalid', 'value')
        args = Arguments()
        with self.assertRaises(Exception):
            banhost.run(args, config)

        # Config with a level as argument
        config = make_config(self.working_dir)
        args = Arguments(log_level='verbose')
        log_file = config.get('logging', 'output')
        with patch('banhost.init_logging') as mock_init_logging:
            banhost.run(args, config)
            mock_init_logging.assert_called_once_with(level='verbose', output=log_file, facility=None)

        assert_not_called(mock_communicate)

    @patch('banhost.init_logging')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_main(self, mock_communicate, mock_logging):
        """Call main."""
        config = make_config(self.working_dir)
        etc_dir = os.path.join(self.working_dir, 'etc')
        os.mkdir(etc_dir)
        config_file = os.path.join(etc_dir, 'banhost.conf')
        with open(config_file, 'w') as outfh:
            config.write(outfh)

        # Call with config file as argument
        with patch('sys.exit') as mock_exit:
            banhost.main(arguments=['--dry-run', '--config-file={}'.format(config_file)])
            assert_not_called(mock_exit)

        # Call without config file
        with patch('sys.exit') as mock_exit:
            no_conf_message = 'no configuration file found\n'
            with patch('sys.stderr') as mock_stderr:
                banhost.main(arguments=['--dry-run'])
                mock_stderr.write.assert_called_once_with(no_conf_message)
            mock_exit.assert_called_once_with(1)

        # Call without config file in debug mode
        with patch('sys.exit') as mock_exit:
            with patch('sys.stderr') as mock_stderr:
                banhost.main(arguments=['--debug', '--dry-run'])
                assert_called(mock_stderr.write)
            mock_exit.assert_called_once_with(1)

        # Call with default config file
        executable = os.path.join(self.working_dir, 'bin', 'banhost')
        with patch('sys.exit') as mock_exit:
            banhost.main(executable=executable, arguments=['--dry-run'])
            assert_not_called(mock_exit)

        assert_not_called(mock_communicate)

    @patch('banhost.init_logging')
    @patch('banhost.netfilter.JsonProcess.communicate', return_value="")
    def test_version(self, mock_communicate, mock_logging):
        """Print version."""
        executable = 'banhost'
        version_string = '{} {}{}'.format(executable, banhost.version.__version__, os.linesep)
        with patch('sys.stdout') as mock_stdout:
            with self.assertRaises(SystemExit):
                banhost.main(executable=executable, arguments=['--version'])
            mock_stdout.write.assert_called_once_with(version_string)
        assert_not_called(mock_communicate)

class TestRunIpTables(unittest.TestCase):

    """Test function run with iptables."""

    def setUp(self):
        self.working_dir = tempfile.mkdtemp(prefix='tmp_banhost')

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.init_logging')
    @patch('banhost.netfilter.Capture.__call__', return_value=0)
    @patch('banhost.netfilter.Call.__call__', return_value=0)
    def test_banhost_host(self, mock_call, mock_capture, mock_init_logging, mock_gethostname, mock_getaddrinfo):
        """Ban an host."""
        config = make_config(self.working_dir, 'iptables')
        # Ban an host
        add_args = Arguments(False, None, None, [TEST_IPV6_ADDRESS1], None, None, False, False, False, False, False, False, False)
        banhost.run(add_args, config)
        self.assertTrue(os.path.isfile(config.get('files', 'database')))
        # Get status
        status_file = os.path.join(self.working_dir, 'status.json')
        status_args = Arguments(False, None, None, [], status_file, None, None, False, False, False, False, False, False)
        with patch('logging.error'):
            banhost.run(status_args, config)
        self.assertTrue(os.path.isfile(status_file))
        status = read_json(status_file)
        self.assertEqual([TEST_IPV6_ADDRESS1], [ host['address'] for host in status['banhost'] ])
        # Calls
        assert_called(mock_gethostname)
        assert_called(mock_getaddrinfo)
        mock_call.assert_has_calls([
            call(['/disable/ip6tables', '--append', 'in_banhost', '--source', TEST_IPV6_ADDRESS1, '--jump', 'BLOCKLIST_INCOMING'], check=True),
            call(['/disable/ip6tables', '--append', 'out_banhost', '--destination', TEST_IPV6_ADDRESS1, '--jump', 'BLOCKLIST_OUTGOING'], check=True) ])
        mock_capture.assert_has_calls([
            call(['/disable/iptables', '--numeric', '--list', 'in_banhost']),
            call(['/disable/ip6tables', '--numeric', '--list', 'in_banhost']),
            call(['/disable/iptables', '--numeric', '--list', 'out_banhost']),
            call(['/disable/ip6tables', '--numeric', '--list', 'out_banhost'])
        ])

    @patch('logging.info')
    @patch('banhost.init_logging')
    @patch('banhost.netfilter.Capture.__call__', return_value=0)
    @patch('banhost.netfilter.Call.__call__', return_value=0)
    def test_arg_database(self, mock_call, mock_capture, mock_init_logging, mock_log_info):
        """Pass database as argument."""
        config = make_config(self.working_dir, 'iptables')
        alt_dbfile = os.path.join(self.working_dir, 'other.db')
        status_file = os.path.join(self.working_dir, 'status.json')
        args = Arguments(False, alt_dbfile, None, [], status_file, None, False, False, False, False, False, False, False)
        banhost.run(args, config)
        mock_log_info.assert_any_call('database: {}'.format(alt_dbfile))


class TestRunNfTables(unittest.TestCase):

    """Test function run with nftables."""

    def setUp(self):
        self.working_dir = tempfile.mkdtemp(prefix='tmp_banhost')

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('socket.getaddrinfo', side_effect=fake_getaddrinfo)
    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.init_logging')
    @patch('banhost.process.JsonProcess.communicate', return_value="")
    def test_banhost_host(self, mock_communicate, mock_init_logging, mock_gethostname, mock_getaddrinfo):
        """Ban an host."""
        config = make_config(self.working_dir)
        # Ban an host
        add_args = Arguments(False, None, None, [TEST_IPV6_ADDRESS1], None, None, False, False, False, False, False, False, False)
        banhost.run(add_args, config)
        self.assertTrue(os.path.isfile(config.get('files', 'database')))
        # Get status
        status_file = os.path.join(self.working_dir, 'status.json')
        status_args = Arguments(False, None, None, [], status_file, None, None, False, False, False, False, False, False)
        with patch('logging.error'):
            banhost.run(status_args, config)
        self.assertTrue(os.path.isfile(status_file))
        status = read_json(status_file)
        self.assertEqual([TEST_IPV6_ADDRESS1], [ host['address'] for host in status['banhost'] ])
        # Calls
        assert_called(mock_gethostname)
        assert_called(mock_getaddrinfo)
        mock_communicate.assert_has_calls([
            call('{"nftables": [{"add": {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "saddr"}}, "right": "fc00:4f8:0:2::69"}}, {"jump": {"target": "BLOCKLIST_INCOMING"}}]}}}, {"add": {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "daddr"}}, "right": "fc00:4f8:0:2::69"}}, {"jump": {"target": "BLOCKLIST_OUTGOING"}}]}}}]}'),
            call('{"nftables": [{"list": {"chain": {"family": "inet", "table": "filter", "name": "in_banhost"}}}]}'),
            call('{"nftables": [{"list": {"chain": {"family": "inet", "table": "filter", "name": "out_banhost"}}}]}')])

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('logging.info')
    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.init_logging')
    @patch('banhost.process.JsonProcess.communicate', return_value="")
    def test_arg_database(self, mock_communicate, mock_init_logging, mock_gethostname, mock_log_info):
        """Pass database as argument."""
        config = make_config(self.working_dir)
        alt_dbfile = os.path.join(self.working_dir, 'other.db')
        status_file = os.path.join(self.working_dir, 'status.json')
        args = Arguments(False, alt_dbfile, None, [], status_file, None, False, False, False, False, False, False, False)
        banhost.run(args, config)
        mock_log_info.assert_any_call('database: {}'.format(alt_dbfile))
        mock_communicate.assert_has_calls([
            call('{"nftables": [{"list": {"chain": {"family": "inet", "table": "filter", "name": "in_banhost"}}}]}'),
            call('{"nftables": [{"list": {"chain": {"family": "inet", "table": "filter", "name": "out_banhost"}}}]}')])

    @patch('logging.error')
    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.process.JsonProcess.communicate')
    def test_status_text(self, mock_communicate, mock_gethostname, mock_log_error):
        """Check that status text is printed."""
        mock_communicate.side_effect = [ LIST_IPV6_CHAIN_IN, LIST_IPV6_CHAIN_OUT ]
        config = make_config(self.working_dir)
        dbname = config.get('files', 'database')
        with contextlib.closing(banhost.core.Database(dbname)) as db:
            event = banhost.core.Event(banhost.core.Host(TEST_IPV6_ADDRESS1, 6), 0, 0)
            db.insert_new_event(event)
        status_args = Arguments(list_hosts=True)
        output = io.StringIO()
        banhost.run(status_args, config, output=output)
        self.assertEqual("Banned hosts:\n{}\n".format(TEST_IPV6_ADDRESS1), output.getvalue())
        assert_called(mock_communicate)

    @patch('logging.error')
    @patch('socket.gethostname', return_value='localhost')
    @patch('banhost.process.JsonProcess.communicate')
    def test_status_text_with_discrepancies(self, mock_communicate, mock_gethostname, mock_log_error):
        """Status text when database and netfilter are out of sync."""
        mock_communicate.side_effect = [ LIST_IPV6_CHAIN_IN, LIST_IPV6_CHAIN_OUT ]
        config = make_config(self.working_dir)
        dbname = config.get('files', 'database')
        with contextlib.closing(banhost.core.Database(dbname)) as db:
            event4 = banhost.core.Event(banhost.core.Host(TEST_IPV4_ADDRESS1, 4), 0, 0)
            db.insert_new_event(event4)
        status_args = Arguments(list_hosts=True)
        output = io.StringIO()
        banhost.run(status_args, config, output=output)
        expected_output = '\n'.join(["Banned hosts:", "{ipv4}",
                                     "Missing in netfilter:", "{ipv4}",
                                     "Remaining in netfilter:", "{ipv6}",
                                     ""]).format(ipv4=TEST_IPV4_ADDRESS1, ipv6=TEST_IPV6_ADDRESS1)
        self.assertEqual(expected_output, output.getvalue())
        assert_called(mock_communicate)

    @patch('logging.error')
    @patch('socket.gethostname', return_value='localhost')
    def test_status_text_with_partial_rules(self, mock_gethostname, mock_log_error):
        """Status text when incoming or outgoing rules are missing but not both."""
        missing_ipv6_chain_in = remove_last_rule(LIST_IPV6_CHAIN_IN)
        missing_ipv6_chain_out = remove_last_rule(LIST_IPV6_CHAIN_OUT)

        config = make_config(self.working_dir)
        dbname = config.get('files', 'database')
        with contextlib.closing(banhost.core.Database(dbname)) as db:
            event6 = banhost.core.Event(banhost.core.Host(TEST_IPV6_ADDRESS1, 4), 0, 0)
            db.insert_new_event(event6)

        status_args = Arguments(list_hosts=True)
        for direction, side_effect in [ ('incoming', [ missing_ipv6_chain_in, LIST_IPV6_CHAIN_OUT ]),
                                        ('outgoing', [ LIST_IPV6_CHAIN_IN, missing_ipv6_chain_out ]) ]:
            with patch('banhost.process.JsonProcess.communicate') as mock_communicate:
                mock_communicate.side_effect = side_effect
                output = io.StringIO()
                banhost.run(status_args, config, output=output)
                expected_output = '\n'.join(["Banned hosts:", "{ipv6}",
                                             "Missing in netfilter:",
                                             "{ipv6} ({direction})",
                                             ""]).format(ipv6=TEST_IPV6_ADDRESS1, direction=direction)
                self.assertEqual(expected_output, output.getvalue())
                assert_called(mock_communicate)

if __name__ == "__main__":
    unittest.main()
