# Copyright (C) 2019 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import banhost.process
import json
import subprocess
import unittest
from unittest.mock import call, patch, Mock, MagicMock

from tests.helpers.asserts import *
from tests.helpers.subproc import FakePopen

TEST_ENV = { 'CALLED_BY': 'me' }

class TestCall(unittest.TestCase):

    """Test class Call."""

    @patch('subprocess.run')
    def test_run(self, mock_run):
        """Call process with custom env"""
        cmdline = [ '/not/a/command' ]
        mock_run.return_value = subprocess.CompletedProcess(cmdline, 0)
        env = TEST_ENV
        proc = banhost.process.Call(env)
        proc(cmdline, check=False)
        mock_run.assert_called_once_with(cmdline, env=env, check=False)

class TestCapture(unittest.TestCase):

    """Test class Capture."""

    @patch('subprocess.Popen')
    def test_run(self, mock_popen):
        """Call process with custom env"""
        blines = b"one\ntwo\n"
        mock_popen.return_value = FakePopen(blines, 0)
        cmdline = [ '/not/a/command' ]
        mock_filter_line = Mock()
        env = TEST_ENV
        proc = banhost.process.Capture(env, mock_filter_line)
        lines = blines.decode('UTF-8').rstrip().split()
        proc(cmdline)
        mock_popen.assert_called_once_with(cmdline, env=env, stdout=subprocess.PIPE)
        mock_filter_line.assert_has_calls([ call(line) for line in lines ])

class TestJsonProcess(unittest.TestCase):

    """Test class JsonProcess."""

    @patch('subprocess.Popen')
    def test_send(self, mock_popen):
        """Send a JSON input and decode JSON output."""
        input_obj = { 'direction': 'in' }
        output_obj = { 'direction': 'out' }
        fake_popen = FakePopen(json.dumps(output_obj), 0)
        mock_popen.return_value = fake_popen
        cmdline = [ '/not/a/command' ]
        env = TEST_ENV
        proc = banhost.process.JsonProcess(cmdline, env)
        result = proc.send(input_obj)
        self.assertEqual(json.dumps(input_obj) + "\n", fake_popen.input)
        mock_popen.assert_called_once_with(cmdline, stdin=subprocess.PIPE, stdout=subprocess.PIPE, env=env, universal_newlines=True)
        self.assertEqual([ output_obj ], result)

    @patch('subprocess.Popen')
    def test_set_args(self, mock_popen):
        """Change command line of JsonProcess."""
        fake_popen = FakePopen("{}", 0)
        mock_popen.return_value = fake_popen
        env = TEST_ENV
        proc = banhost.process.JsonProcess([ '/wrong/command' ], env)
        cmdline = [ '/correct/command' ]
        proc.set_args(cmdline)
        result = proc.send({})
        mock_popen.assert_called_once_with(cmdline, stdin=subprocess.PIPE, stdout=subprocess.PIPE, env=env, universal_newlines=True)

    @patch('subprocess.Popen')
    def test_command_error(self, mock_popen):
        """Exception raised on error."""
        fake_popen = FakePopen("{}", 1)
        mock_popen.return_value = fake_popen
        proc = banhost.process.JsonProcess([ '/not/a/command' ], TEST_ENV)
        with self.assertRaises(banhost.process.JsonProcessError):
            result = proc.send({})

    @patch('banhost.process.JsonProcess.communicate')
    def test_dry_run(self, mock_communicate):
        """Dry run."""
        proc = banhost.process.JsonProcess([ '/not/a/command' ], TEST_ENV)
        result = proc.send({}, dry_run=True)
        self.assertEqual([], result)
        assert_not_called(mock_communicate)

if __name__ == '__main__':
    unittest.main()
