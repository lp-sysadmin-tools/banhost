# Copyright (C) 2018 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import socket

__all__ = [ 'fake_gethostbyaddr', 'fake_getaddrinfo', 'fake_gethostname',
            'TEST_IPV4_ADDRESS1', 'TEST_IPV4_ADDRESS2', 'TEST_IPV4_ADDRESS3',
            'TEST_IPV6_ADDRESS1', 'TEST_IPV6_ADDRESS2', 'TEST_IPV6_ADDRESS3',
            'TEST_HOST_NAME1', 'TEST_HOST_NAME2' ]

TEST_IPV4_ADDRESS1 = '198.51.100.69'
TEST_IPV4_ADDRESS2 = '198.51.100.179'
TEST_IPV4_ADDRESS3 = '198.51.100.141'

TEST_IPV6_ADDRESS1 = 'fc00:4f8:0:2::69'
TEST_IPV6_ADDRESS2 = 'fc00:4f8:0:2::70'
TEST_IPV6_ADDRESS3 = 'fc00:4f8:0:2::71'

TEST_HOST_NAME1 = 'host1.somewhere.net'
TEST_HOST_NAME2 = 'host2.elsewhere.net'

def fake_gethostname(addr):
    names = {
        TEST_IPV4_ADDRESS1: TEST_HOST_NAME1,
        TEST_IPV6_ADDRESS1: TEST_HOST_NAME1,
        TEST_IPV6_ADDRESS2: TEST_HOST_NAME2
    }
    try:
        return names[addr]
    except KeyError:
        return 'host{}.nowhere.net'.format(addr.replace('.', '_'))

def fake_gethostbyaddr(addr):
    if addr == TEST_IPV4_ADDRESS1 or addr == TEST_IPV6_ADDRESS1:
        return (TEST_HOST_NAME1, [], [addr])
    elif addr == TEST_IPV6_ADDRESS2:
        return (TEST_HOST_NAME2, [], [addr])
    raise socket.herror('{}: not found'.format(addr))

def fake_getaddrinfo(hostname, *args, **kw):
    if hostname == TEST_HOST_NAME1:
        return [(socket.AF_INET6, socket.SOCK_DGRAM, 17, '', (TEST_IPV6_ADDRESS1, 0, 0, 0)),
                (socket.AF_INET, socket.SOCK_DGRAM, 17, '', (TEST_IPV4_ADDRESS1, 0))]
    elif hostname == 'localhost':
        return [(socket.AF_INET6, socket.SOCK_DGRAM, 17, '', ('::1', 0, 0, 0)),
                (socket.AF_INET, socket.SOCK_DGRAM, 17, '', ('127.0.0.1', 0))]
    elif hostname == TEST_IPV4_ADDRESS1:
        return [(socket.AF_INET, socket.SOCK_DGRAM, 17, '', (TEST_IPV4_ADDRESS1, 0))]
    elif hostname in [ TEST_IPV6_ADDRESS1, TEST_IPV6_ADDRESS2, TEST_IPV6_ADDRESS3 ]:
        return [(socket.AF_INET6, socket.SOCK_DGRAM, 17, '', (hostname, 0))]
    raise Exception('{}: not found'.format(hostname))
