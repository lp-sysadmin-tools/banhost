# Copyright (C) 2019 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io

__all__ = [ 'FakePopen' ]

class FakePopen:

    def __init__(self, output, retcode):
        if isinstance(output, bytes):
            self.stdout = io.BytesIO(output)
        else:
            self.stdout = io.StringIO(output)
        self.retcode = retcode
        self.input = None

    def communicate(self, input=None):
        self.input = input
        return self.stdout.getvalue(), None

    def wait(self, **kw):
        self.stdout.close()
        return self.retcode
