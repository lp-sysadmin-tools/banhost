# Copyright (C) 2018-2019 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import banhost.netfilter
import io
import json
import os
import stat
import subprocess
import sys
import tempfile
import unittest

from unittest.mock import call, patch
from banhost.network import Host

from tests.helpers.asserts import *
from tests.helpers.dns import *
from tests.helpers.subproc import FakePopen

HOSTS_4 = sorted([ Host(TEST_IPV4_ADDRESS1, 4), Host(TEST_IPV4_ADDRESS2, 4), Host(TEST_IPV4_ADDRESS3, 4) ])

LIST_INCOMING_4 = b"""Chain cin (1 references)
target     prot opt source               destination
INCOMING   all  --  198.51.100.69        0.0.0.0/0
INCOMING   all  --  198.51.100.179       0.0.0.0/0
INCOMING   all  --  198.51.100.141       0.0.0.0/0
"""

LIST_OUTGOING_4 = b"""Chain cout (1 references)
target     prot opt source               destination
OUTGOING   all  --  0.0.0.0/0            198.51.100.69
OUTGOING   all  --  0.0.0.0/0            198.51.100.179
OUTGOING   all  --  0.0.0.0/0            198.51.100.141
"""

HOSTS_6 = sorted([ Host(TEST_IPV6_ADDRESS1, 6) ])

LIST_INCOMING_6 = b"""Chain cin (1 references)
target     prot opt source               destination
INCOMING  all      fc00:4f8:0:2::69  ::/0
"""

LIST_OUTGOING_6 = b"""Chain cout (1 references)
target     prot opt source               destination
OUTGOING  all      ::/0                 fc00:4f8:0:2::69
"""

HOSTS = sorted(HOSTS_4 + HOSTS_6)

def make_executable_files(*filenames):
    """Create an executable empty file"""
    for filename in filenames:
        with io.open(filename, 'w'):
            pass
        os.chmod(filename, stat.S_IRWXU)

class TestFunctions(unittest.TestCase):

    """Test utilitary functions."""

    def test_few_items_string(self):
        """Test function few_items_string"""
        few_items_string = banhost.netfilter.few_items_string
        self.assertEqual('a', few_items_string(['a']))
        self.assertEqual('a, b', few_items_string(['a', 'b']))
        self.assertEqual('a, b, ...', few_items_string(['a', 'b', 'c']))
        self.assertEqual('a, b, c', few_items_string(['a', 'b', 'c'], max=3))

class TestHostMatcher(unittest.TestCase):

    """Test IpTables.HostMatcher"""

    def test_incoming_v4(self):
        """Match incoming list IPv4."""
        collect = banhost.netfilter.IpTables.HostMatcher('INCOMING', True, 4)
        for line in LIST_INCOMING_4.decode('UTF-8').split('\n'):
            collect(line)
        self.assertEqual(HOSTS_4, sorted(collect.hosts))

    def test_outgoing_v4(self):
        """Match outgoing list IPv4."""
        collect = banhost.netfilter.IpTables.HostMatcher('OUTGOING', False, 4)
        for line in LIST_OUTGOING_4.decode('UTF-8').split('\n'):
            collect(line)
        self.assertEqual(HOSTS_4, sorted(collect.hosts))

    def test_incoming_v6(self):
        """Match incoming list IPv6."""
        collect = banhost.netfilter.IpTables.HostMatcher('INCOMING', True, 6)
        for line in LIST_INCOMING_6.decode('UTF-8').split('\n'):
            collect(line)
        self.assertEqual(HOSTS_6, sorted(collect.hosts))

    def test_outgoing_v6(self):
        """Match outgoing list IPv6."""
        collect = banhost.netfilter.IpTables.HostMatcher('OUTGOING', False, 6)
        for line in LIST_OUTGOING_6.decode('UTF-8').split('\n'):
            collect(line)
        self.assertEqual(HOSTS_6, sorted(collect.hosts))

class TestIpTables(unittest.TestCase):

    """Test iptables front-end."""

    CHAIN_IN = 'cin'
    TARGET_IN = 'INCOMING'
    CHAIN_OUT =  'cout'
    TARGET_OUT =  'OUTGOING'

    def iptable_command(self, host):
        ipversion = '' if host.ipversion == 4 else host.ipversion
        return '/sbin/ip{}tables'.format(ipversion)

    def calls(self, command, host, check=True):
        executable = self.iptable_command(host)
        option = '--{}'.format(command)
        cin = [executable, option, self.CHAIN_IN, '--source', host.address, '--jump', self.TARGET_IN]
        cout = [executable, option, self.CHAIN_OUT, '--destination', host.address, '--jump', self.TARGET_OUT]
        return [ call(cin, check=check, env={}), call(cout, check=check, env={}) ]

    @patch('subprocess.run', return_value=subprocess.CompletedProcess([], 0))
    def test_append(self, mock_run):
        """Append rule"""
        iptables = banhost.netfilter.IpTables(self.CHAIN_IN, self.TARGET_IN, self.CHAIN_OUT, self.TARGET_OUT, False)
        iptables.child_env = {}
        host4 = Host(TEST_IPV4_ADDRESS1, 4)
        host6 = Host(TEST_IPV6_ADDRESS1, 6)
        iptables.append(host4)
        iptables.append(host6)
        mock_run.assert_has_calls(self.calls('append', host4) + self.calls('append', host6))

    @patch('subprocess.run', return_value=subprocess.CompletedProcess([], 0))
    def test_delete(self, mock_run):
        """Delete rule"""
        iptables = banhost.netfilter.IpTables(self.CHAIN_IN, self.TARGET_IN, self.CHAIN_OUT, self.TARGET_OUT, False)
        iptables.child_env = {}
        host4 = Host(TEST_IPV4_ADDRESS1, 4)
        host6 = Host(TEST_IPV6_ADDRESS1, 6)
        iptables.delete(host4)
        iptables.delete(host6)
        mock_run.assert_has_calls(self.calls('delete', host4, check=False) + self.calls('delete', host6, check=False))

    @patch('subprocess.Popen')
    def test_list_in(self, mock_popen):
        """List chain in"""
        iptables = banhost.netfilter.IpTables(self.CHAIN_IN, self.TARGET_IN, self.CHAIN_OUT, self.TARGET_OUT, False)
        iptables.child_env = {}
        mock_popen.side_effect = [ FakePopen(LIST_INCOMING_4, 0), FakePopen(LIST_INCOMING_6, 0) ]
        hosts = iptables.list_chain_in()
        self.assertEqual(HOSTS, sorted(hosts))
        mock_popen.assert_has_calls([call(['/sbin/iptables', '--numeric', '--list', self.CHAIN_IN], env={}, stdout=-1),
                                     call(['/sbin/ip6tables', '--numeric', '--list', self.CHAIN_IN], env={}, stdout=-1)])

    @patch('subprocess.Popen')
    def test_list_in_failure(self, mock_popen):
        """List chain in failure"""
        iptables = banhost.netfilter.IpTables(self.CHAIN_IN, self.TARGET_IN, self.CHAIN_OUT, self.TARGET_OUT, False)
        iptables.child_env = {}
        mock_popen.side_effect = [ FakePopen(LIST_INCOMING_4, 1), FakePopen(LIST_INCOMING_6, 0) ]
        with self.assertRaises(subprocess.CalledProcessError):
            iptables.list_chain_in()
        mock_popen.assert_called_once_with(['/sbin/iptables', '--numeric', '--list', self.CHAIN_IN], env={}, stdout=-1)

    @patch('subprocess.Popen')
    def test_list_out(self, mock_popen):
        """List chain out"""
        iptables = banhost.netfilter.IpTables(self.CHAIN_IN, self.TARGET_IN, self.CHAIN_OUT, self.TARGET_OUT, False)
        iptables.child_env = {}
        mock_popen.side_effect = [ FakePopen(LIST_OUTGOING_4, 0), FakePopen(LIST_OUTGOING_6, 0) ]
        hosts = iptables.list_chain_out()
        self.assertEqual(HOSTS, sorted(hosts))
        mock_popen.assert_has_calls([call(['/sbin/iptables', '--numeric', '--list', self.CHAIN_OUT], env={}, stdout=-1),
                                     call(['/sbin/ip6tables', '--numeric', '--list', self.CHAIN_OUT], env={}, stdout=-1)])

    def test_is_available(self):
        """Check if framework is available"""
        iptables = banhost.netfilter.IpTables(self.CHAIN_IN, self.TARGET_IN, self.CHAIN_OUT, self.TARGET_OUT, False)
        with tempfile.TemporaryDirectory() as working_dir:
            command4 = os.path.join(working_dir, 'fake4')
            iptables.set_command(command4, 4)
            command6 = os.path.join(working_dir, 'fake6')
            iptables.set_command(command6, 6)
            self.assertFalse(iptables.is_available())
            make_executable_files(command4, command6)
            self.assertTrue(iptables.is_available())


ADD_IPV4_ADDRESS = """{"nftables": [{"add": {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip", "field": "saddr"}}, "right": "__IP__"}}, {"jump": {"target": "pr_in_banhost"}}]}}}, {"add": {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip", "field": "daddr"}}, "right": "__IP__"}}, {"jump": {"target": "pr_out_banhost"}}]}}}]}""".replace('__IP__', TEST_IPV4_ADDRESS1)

REMOVE_IPV4_ADDRESS = """{"nftables": [{"delete": {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "handle": 35}}}, {"delete": {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "handle": 36}}}]}"""

LIST_IPV4_RULES = """{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram", "json_schema_version": 1}}, {"chain": {"family": "inet", "table": "filter", "name": "in_banhost", "handle": 25}}, {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "handle": 35, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip", "field": "saddr"}}, "right": "__IP__"}}, {"jump": {"target": "pr_in_banhost"}}]}}]}
{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram", "json_schema_version": 1}}, {"chain": {"family": "inet", "table": "filter", "name": "out_banhost", "handle": 26}}, {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "handle": 36, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip", "field": "daddr"}}, "right": "__IP__"}}, {"jump": {"target": "pr_out_banhost"}}]}}]}""".replace('__IP__', TEST_IPV4_ADDRESS1)

ADD_IPV6_ADDRESS = """{"nftables": [{"add": {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "saddr"}}, "right": "__IP__"}}, {"jump": {"target": "pr_in_banhost"}}]}}}, {"add": {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "daddr"}}, "right": "__IP__"}}, {"jump": {"target": "pr_out_banhost"}}]}}}]}""".replace('__IP__', TEST_IPV6_ADDRESS1)

REMOVE_IPV6_ADDRESS = """{"nftables": [{"delete": {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "handle": 33}}}, {"delete": {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "handle": 34}}}]}"""

LIST_IPV6_CHAIN_IN = """{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram", "json_schema_version": 1}}, {"chain": {"family": "inet", "table": "filter", "name": "in_banhost", "handle": 25}}, {"rule": {"family": "inet", "table": "filter", "chain": "in_banhost", "handle": 33, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "saddr"}}, "right": "__IP__"}}, {"jump": {"target": "pr_in_banhost"}}]}}]}
""".replace('__IP__', TEST_IPV6_ADDRESS1)

LIST_IPV6_CHAIN_OUT = """{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram", "json_schema_version": 1}}, {"chain": {"family": "inet", "table": "filter", "name": "out_banhost", "handle": 26}}, {"rule": {"family": "inet", "table": "filter", "chain": "out_banhost", "handle": 34, "expr": [{"match": {"op": "==", "left": {"payload": {"protocol": "ip6", "field": "daddr"}}, "right": "__IP__"}}, {"jump": {"target": "pr_out_banhost"}}]}}]}""".replace('__IP__', TEST_IPV6_ADDRESS1)

LIST_IPV6_RULES = LIST_IPV6_CHAIN_IN + LIST_IPV6_CHAIN_OUT

UNSUPPORTED_NFT_VERSION = """{"nftables": [{"metainfo": {"version": "9.9.9", "release_name": "Unknown", "json_schema_version": 999}}]}"""

UNSUPPORTED_NFT_COMMAND = """{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram", "json_schema_version": 1}}, {"unsupported": {}}]}"""

MISSING_ENTRY_IN_NFT_COMMAND = """{"nftables": [{"metainfo": {"version": "0.9.2", "release_name": "Scram"}}]}"""


class TestNftJsonResponse(unittest.TestCase):

    """Test class NftJsonResponse"""

    def test_get_host(self):
        """Test get_host"""
        nft_object = json.loads(LIST_IPV6_CHAIN_IN)
        response = banhost.netfilter.NftJsonResponse(nft_object)
        host = Host(TEST_IPV6_ADDRESS1, 6)
        self.assertEqual([ host ], response.get_hosts('saddr'))

    def test_list_handles(self):
        """Test list_handles"""
        nft_object = json.loads(LIST_IPV6_CHAIN_IN)
        response = banhost.netfilter.NftJsonResponse(nft_object)
        self.assertEqual([ ('in_banhost', 33) ], response.list_handles(TEST_IPV6_ADDRESS1))

    @patch('logging.error')
    def test_bad_rule(self, mock_log_error):
        """Test bad rule"""
        nft_object = json.loads(LIST_IPV6_CHAIN_IN)
        del nft_object['nftables'][-1]['rule']['chain']
        response = banhost.netfilter.NftJsonResponse(nft_object)
        self.assertEqual([], response.list_handles(TEST_IPV6_ADDRESS1))
        mock_log_error.assert_called_once()

class TestNfTables(unittest.TestCase):

    """Test nftables front-end."""

    FAMILY = 'inet'
    TABLE = 'filter'
    CHAIN_IN = 'in_banhost'
    TARGET_IN = 'pr_in_banhost'
    CHAIN_OUT =  'out_banhost'
    TARGET_OUT =  'pr_out_banhost'

    def new_instance(self):
        nftables = banhost.netfilter.NfTables(self.FAMILY, self.TABLE,
                                                self.CHAIN_IN, self.TARGET_IN,
                                                self.CHAIN_OUT, self.TARGET_OUT, False)
        nftables.child_env = {}
        return nftables

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('banhost.process.JsonProcess.communicate', return_value='')
    def test_append_ipv4(self, mock_communicate):
        """Append IPv4 rule"""
        nftables = self.new_instance()
        host = Host(TEST_IPV4_ADDRESS1, 4)
        nftables.append(host)
        mock_communicate.assert_called_once_with(ADD_IPV4_ADDRESS)

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('banhost.process.JsonProcess.communicate', return_value='')
    def test_append_ipv6(self, mock_communicate):
        """Append IPv6 rule"""
        nftables = self.new_instance()
        host = Host(TEST_IPV6_ADDRESS1, 6)
        nftables.append(host)
        mock_communicate.assert_called_once_with(ADD_IPV6_ADDRESS)

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('banhost.process.JsonProcess.communicate')
    def test_delete_ipv4(self, mock_communicate):
        """Append IPv4 rule"""
        mock_communicate.side_effect = [ LIST_IPV4_RULES, '' ]
        nftables = self.new_instance()
        host = Host(TEST_IPV4_ADDRESS1, 4)
        nftables.delete(host)
        mock_communicate.assert_has_calls([call(REMOVE_IPV4_ADDRESS)])

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('banhost.process.JsonProcess.communicate')
    def test_delete_ipv6(self, mock_communicate):
        """Append IPv6 rule"""
        mock_communicate.side_effect = [ LIST_IPV6_RULES, '' ]
        nftables = self.new_instance()
        host = Host(TEST_IPV6_ADDRESS1, 6)
        nftables.delete(host)
        mock_communicate.assert_has_calls([call(REMOVE_IPV6_ADDRESS)])

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('logging.error')
    @patch('banhost.netfilter.JsonProcess.communicate')
    def test_delete_unknown(self, mock_communicate, mock_log_error):
        mock_communicate.side_effect = [ LIST_IPV6_RULES, '' ]
        nftables = self.new_instance()
        host = Host(TEST_IPV6_ADDRESS2, 6)
        nftables.delete(host)
        mock_log_error.assert_has_calls([ call("{}: no rule matching host".format(TEST_IPV6_ADDRESS2)),
                                          call("{}: nothing to delete".format(TEST_IPV6_ADDRESS2)) ])

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('logging.error')
    @patch('subprocess.Popen')
    def test_delete_error(self, mock_popen, mock_log_error):
        mock_popen.side_effect = [ FakePopen(LIST_IPV6_RULES, 0), FakePopen('', 1) ]
        nftables = self.new_instance()
        host = Host(TEST_IPV6_ADDRESS1, 6)
        nftables.delete(host)
        mock_log_error.assert_called_once_with("{}: deletion failed".format(TEST_IPV6_ADDRESS1))

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('banhost.process.JsonProcess.communicate', return_value='')
    def test_append_list_chain_in(self, mock_communicate):
        """Append IPv6 rule"""
        mock_communicate.side_effect = [ LIST_IPV6_CHAIN_IN ]
        nftables = self.new_instance()
        chain = nftables.list_chain_in()
        host = Host(TEST_IPV6_ADDRESS1, 6)
        self.assertEqual([ host ], chain)
        mock_communicate.assert_called_once()

    @unittest.skipIf(sys.version_info[0:2] < (3, 7), "not supported without ordered dictionaries")
    @patch('banhost.process.JsonProcess.communicate', return_value='')
    def test_append_list_chain_out(self, mock_communicate):
        """Append IPv6 rule"""
        mock_communicate.side_effect = [ LIST_IPV6_CHAIN_OUT ]
        nftables = self.new_instance()
        chain = nftables.list_chain_out()
        host = Host(TEST_IPV6_ADDRESS1, 6)
        self.assertEqual([ host ], chain)
        mock_communicate.assert_called_once()

    @patch('banhost.process.JsonProcess.communicate')
    def test_unsupported_nft_version(self, mock_communicate):
        """Test exception on unsupported nft"""
        mock_communicate.side_effect = [ UNSUPPORTED_NFT_VERSION ]
        nftables = self.new_instance()
        with self.assertRaises(banhost.netfilter.InvalidJsonError):
            nftables.list_chain_in()

    @patch('banhost.process.JsonProcess.communicate')
    def test_unsupported_nft_command(self, mock_communicate):
        """Test exception on unsupported nft command"""
        mock_communicate.side_effect = [ UNSUPPORTED_NFT_COMMAND ]
        nftables = self.new_instance()
        with self.assertRaises(banhost.netfilter.InvalidJsonError):
            nftables.list_chain_in()

    @patch('banhost.process.JsonProcess.communicate')
    def test_empty_response(self, mock_communicate):
        """Test exception on empty response"""
        mock_communicate.side_effect = [ """{"nftables": []}""" ]
        nftables = self.new_instance()
        with self.assertRaises(banhost.netfilter.InvalidJsonError):
            nftables.list_chain_in()

    @patch('banhost.process.JsonProcess.communicate')
    def test_missing_nft_entry(self, mock_communicate):
        """Test exception on missing entry"""
        mock_communicate.side_effect = [ MISSING_ENTRY_IN_NFT_COMMAND ]
        nftables = self.new_instance()
        with self.assertRaises(banhost.netfilter.InvalidJsonError):
            nftables.list_chain_in()

    def test_is_available(self):
        """Check if framework is available"""
        nftables = self.new_instance()
        with tempfile.TemporaryDirectory() as working_dir:
            command = os.path.join(working_dir, 'fake')
            nftables.set_command([ command ])
            self.assertFalse(nftables.is_available())
            make_executable_files(command)
            self.assertTrue(nftables.is_available())

if __name__ == '__main__':
    unittest.main()
