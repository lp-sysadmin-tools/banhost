Banhost
=======

[![build status](https://gitlab.com/lp-sysadmin-tools/banhost/badges/master/pipeline.svg)](https://gitlab.com/lp-sysadmin-tools/banhost/commits/master)
[![license](https://img.shields.io/badge/license-GPL3-lightgrey.svg)](https://gitlab.com/lp-sysadmin-tools/banhost/raw/master/LICENSE)

Add netfilter rules to ban hosts. The list of banned hosts is kept in a SQLite database.

It is meant to be used with [Slogguard](https://gitlab.com/lp-sysadmin-tools/slogguard).

Features:

- Support IPv6.

- Quarantine delay increases if host is blocked repeatedly.

- Can generate a HTML page that lists all blocked hosts.

Use option --help for details.

NfTables
--------

With nft the chains containing the incoming and outgoing blocked hosts can be created with

    nft add chain inet filter in_blocklist
    nft add chain inet filter out_blocklist

The target to process blocked host:

    nft add chain inet filter pr_in_blocklist
    nft add rule inet filter pr_in_blocklist drop

    nft add chain inet filter pr_out_blocklist
    nft add rule inet filter pr_out_blocklist ip protocol tcp reject with tcp reset
    nft add rule inet filter pr_out_blocklist reject with icmpx type no-route
    nft add rule inet filter pr_out_blocklist drop

