# Copyright (C) 2005-2018 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io
import logging
import os
import sqlite3

from collections import namedtuple

from .network import Host, Event, gethostname
from .sql import *

class DatabaseError(Exception):
    pass

class HostTable(Table):

    """Interface to host table."""

    COLUMNS = [('id', 'integer'), ('address', 'text'), ('ipversion', 'integer'), ('name', 'text')]

    def __init__(self, name, primary=None, unique=[]):
        super().__init__(name, self.COLUMNS, primary=primary, unique=unique)

    @staticmethod
    def make(row):
        """Create a host object from a database row (address, ipversion, name)."""
        if not row:
            return None
        return Host(*tuple(row))

class EventTable(Table):

    """Interface to event table."""

    COLUMNS = [('host', 'integer'), ('start_time', 'integer'), ('end_time', 'integer')]

    def __init__(self, name, primary=None, unique=[]):
        super().__init__(name, self.COLUMNS, primary=primary, unique=unique)

    @staticmethod
    def make(row):
        """Create an event object from a database row (address, ipversion, name, start_time, end_time).

        The row is supposed to come from a join with the host table."""
        if not row:
            return None
        return Event(HostTable.make(row[0:3]), row[3], row[4])

def delete_event_stmt_builder(events_table):
    return (StatementBuilder()
            .delete()
            .fromTable(events_table))

def select_events_stmt_builder(hosts_table, events_table):
    return (StatementBuilder()
            .select('h.address', 'h.ipversion', 'h.name', 'e.start_time', 'e.end_time', 'e.host')
            .fromTable(hosts_table, 'h')
            .fromTable(events_table, 'e')
            .where(EqualTo('h.id', 'e.host')))

def make_hosts_statements(tables):
    HostStatements = namedtuple('HostStatements', ('delete_unused', 'get', 'insert'))
    return HostStatements(
        # delete_unused
        StatementBuilder()
        .delete()
        .fromTable(tables.hosts)
        .where(IsNotIn('id', Union(StatementBuilder().select('host').fromTable(tables.blocklist).get(),
                                   StatementBuilder().select('host').fromTable(tables.history).get())))
        .get(),
        # get
        StatementBuilder()
        .select('*')
        .fromTable(tables.hosts)
        .where(EqualTo('address', Text('address')))
        .get(),
        # insert
        StatementBuilder()
        .insertInto(tables.hosts)
        .values(Text('address'), Integer('ipversion'), Text('name'))
        .get()
    )

def make_events_statements(hosts_table, events_table):
    EventStatements = namedtuple('EventStatements',
                                 ('count_by_address',
                                  'delete_all', 'delete_by_hosts', 'delete_old',
                                  'get_all', 'get_by_address', 'get_expired',
                                  'insert'))
    return EventStatements(
        # count_by_address
        StatementBuilder()
        .count()
        .fromTable(hosts_table, 'h').fromTable(events_table, 'p')
        .where(And(EqualTo('h.id', 'p.host'),
                   EqualTo('h.address', Text('address'))))
        .get(),
        # delete_all
        delete_event_stmt_builder(events_table).get(),
        # delete_by_hosts
        delete_event_stmt_builder(events_table).where(IsIn('host', List('hosts'))).get(),
        # delete_old
        delete_event_stmt_builder(events_table)
        .where(LessThanOrEqualTo('start_time', Integer('timestamp')))
        .get(),
        # get_all
        select_events_stmt_builder(hosts_table, events_table)
        .orderBy('e.start_time')
        .get(),
        # get_by_address
        select_events_stmt_builder(hosts_table, events_table)
        .where(And(EqualTo('h.id', 'e.host'),
                   EqualTo('h.address', Text('address'))))
        .get(),
        # get_expired
        select_events_stmt_builder(hosts_table, events_table)
        .where(And(EqualTo('h.id', 'e.host'),
                   LessThanOrEqualTo('e.end_time', Integer('timestamp'))))
        .get(),
        # insert
        StatementBuilder()
        .insertInto(events_table)
        .values(Integer('host'), Integer('start'), Integer('end'))
        .get()
    )

class Transaction:

    """Create a cursor and commit the transaction."""

    def __init__(self, connection):
        self.connection = connection
        self.cursor = None

    def __enter__(self):
        self.cursor = self.connection.cursor()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.cursor:
            self.cursor.close()
            self.cursor = None
            if self.connection and not exc_type:
                self.connection.commit()
                self.connection = None

    def execute(self, statement):
        logging.debug('executing statement: {}'.format(statement))
        try:
            return self.cursor.execute(*statement)
        except sqlite3.Error as ex:
            raise DatabaseError('invalid statement: {}'.format(statement)) from ex

    def fetch_one(self, statement):
        """Fetch at most one row."""
        res = self.execute(statement)
        rows = res.fetchall()
        if len(rows) > 1:
            raise DatabaseError('{}: multiple entries'.format(statement[0]))
        elif not rows:
            return None
        return rows[0]

    def fetch_single(self, statement):
        """Fetch a single value."""
        row = self.fetch_one(statement)
        return row[0] if row else None

    def create_table(self, table):
        """Create the table."""
        return self.execute(table.schema())

class Connection:

    """Front-end to SQLite connection."""

    ColumnInfo = namedtuple('ColumnInfo', ('index', 'name', 'type', 'nullable', 'defval', 'pkey_index'))

    def __init__(self, filename):
        self.filename = filename
        self.exists =  os.path.isfile(filename)
        self.connection = sqlite3.connect(self.filename, isolation_level='EXCLUSIVE')

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def transaction(self):
        return Transaction(self.connection)

    def close(self):
        if self.connection:
            self.connection.close()
            self.connection = None

    def table_exists(self, table):
        """Check if a table exists."""
        with self.transaction() as transaction:
            name = transaction.fetch_single(("SELECT name FROM sqlite_master WHERE type='table' AND name = ?", (table.name,)))
            return name != None

    def table_info(self, table):
        """Return the list of column info."""
        result = []
        with self.transaction() as transaction:
            for row in transaction.execute(("PRAGMA table_info({})".format(table.name),)):
                result.append(self.ColumnInfo(*row))
        return result

class Database:

    """Access SQLite database."""

    TableTuple = namedtuple('TableTuple', ('blocklist', 'history', 'hosts'))

    TABLES = TableTuple(EventTable('blocklist', primary='host'),
                        EventTable('history'),
                        HostTable('hosts', primary='id', unique=['address']))

    def __init__(self, filename):
        self.connection = Connection(filename)
        if not self.connection.exists:
            self.create_tables()
        tables = self.TABLES
        # Hosts statements
        self.hosts = make_hosts_statements(tables)
        # Current events statements
        self.current_events = make_events_statements(tables.hosts, tables.blocklist)
        # Current events statements
        self.past_events = make_events_statements(tables.hosts, tables.history)

    def create_tables(self):
        """Create missing tables"""
        try:
            with self.connection.transaction() as transaction:
                for table in self.TABLES:
                    logging.info("creating table {}".format(table.name))
                    transaction.create_table(table)
        except Exception as ex:
            raise DatabaseError("{}: cannot create tables".format(self.connection.filename)) from ex

    def get_host(self, address):
        """Get the host with the given address or None."""
        with self.connection.transaction() as transaction:
            row = transaction.fetch_one(self.hosts.get(address=address))
            return HostTable.make(row[1:]) if row else None

    def get_current_event_for(self, address):
        """Get event for a single host by IP address."""
        with self.connection.transaction() as transaction:
            row = transaction.fetch_one(self.current_events.get_by_address(address=address))
            return EventTable.make(row)

    def iterate_current_events(self):
        """Return an iterator on current events."""
        with self.connection.transaction() as transaction:
            for row in transaction.execute(self.current_events.get_all()):
                yield EventTable.make(row)

    def collect_current_events(self):
        """Return the list of current events"""
        return [ event for event in self.iterate_current_events() ]

    def delete_current_event(self, address):
        """Delete a single event by IP address."""
        with self.connection.transaction() as transaction:
            row = transaction.fetch_one(self.current_events.get_by_address(address=address))
            if not row:
                raise DatabaseError("{}: no event for this address".format(address))
            host_id = row[5]
            transaction.execute(self.current_events.delete_by_hosts(hosts=[host_id]))
            return EventTable.make(row)

    def delete_expired_events(self, timestamp, keep=False):
        """Delete expired events that have ended before TIMESTAMP and return them."""
        events = []
        hosts = []
        with self.connection.transaction() as transaction:
            for row in transaction.execute(self.current_events.get_expired(timestamp=int(timestamp))):
                events.append(EventTable.make(row))
                hosts.append(row[5])
            transaction.execute(self.current_events.delete_by_hosts(hosts=hosts))
            if keep:
                for index, event in enumerate(events):
                    host_id = hosts[index]
                    transaction.execute(self.past_events.insert(host=host_id, start=event.start_time, end=event.end_time))
        return events

    def clear_current_events(self):
        """Delete all current events."""
        with self.connection.transaction() as transaction:
            transaction.execute(self.current_events.delete_all())

    def insert_new_event(self, event):
        """Insert a new event."""
        with self.connection.transaction() as transaction:
            try:
                host_row = transaction.fetch_one(self.hosts.get(address=event.host.address))
                if not host_row:
                    host = event.host
                    host.resolve()
                    res = transaction.execute(self.hosts.insert(address=host.address,
                                                                ipversion=host.ipversion,
                                                                name=host.name))
                    host_id = res.lastrowid
                else:
                    host_id = host_row[0]
                    host = HostTable.make(host_row[1:])
                    event = Event(host, event.start_time, event.end_time)
                transaction.execute(self.current_events.insert(host=host_id, start=event.start_time, end=event.end_time))
            except DatabaseError as ex:
                event = self.get_current_event_for(event.host.address)
                if not event:
                    raise
        return event

    def iterate_past_events(self):
        """Return an iterator on past events."""
        with self.connection.transaction() as transaction:
            for row in transaction.execute(self.past_events.get_all()):
                yield EventTable.make(row)

    def collect_past_events(self):
        """Return the list of past events"""
        return [ event for event in self.iterate_past_events() ]

    def delete_past_events_older_than(self, timestamp):
        """Delete history events older than TIMESTAMP."""
        with self.connection.transaction() as transaction:
            transaction.execute(self.past_events.delete_old(timestamp=int(timestamp)))

    def delete_hosts_without_events(self):
        """Delete hosts that don't appear in any events."""
        with self.connection.transaction() as transaction:
            transaction.execute(self.hosts.delete_unused())

    def count_past_events_for(self, address):
        """Count history events for a given address."""
        with self.connection.transaction() as transaction:
            return transaction.fetch_single(self.past_events.count_by_address(address=address))

    def vacuum(self):
        """Repacking the database into a minimal amount of disk space."""
        with self.connection.transaction() as transaction:
            return transaction.execute(('VACUUM',))

    def close(self):
        """Close the connection."""
        if self.connection:
            self.connection.close()
            self.connection = None

class DatabaseUpgrade:

    """Upgrade the database to the latest schema."""

    def __init__(self, filename):
        self.filename = filename
        self.exists = os.path.isfile(filename)

    def migrate_events_v4_to_v5(self, connection, host_ids, hosts_table, events_table):
        """Migrate one event table from version 4 to version 5."""
        table_info = connection.table_info(events_table)
        if not table_info or table_info[0].name == 'host':
            return False
        insert_host = (StatementBuilder()
                       .insertInto(hosts_table)
                       .values(Text('address'), Integer('ipversion'), '')
                       .get());
        insert_event = (StatementBuilder()
                        .insertInto(events_table)
                        .values(Integer('host'), Integer('start'), Integer('end'))
                        .get());
        events = []
        EventRow = namedtuple('EventRow', ('host', 'start_time', 'end_time'))
        with connection.transaction() as transaction:
            for row in transaction.execute(('SELECT * FROM {}'.format(events_table.name),)).fetchall():
                address, ipversion, start_time, end_time = row
                if not address in host_ids:
                    res = transaction.execute(insert_host(address=address, ipversion=ipversion))
                    host_id = host_ids[address] = res.lastrowid
                else:
                    host_id = host_ids[address]
                events.append(EventRow(host_id, start_time, end_time))
            logging.info("recreate table {}".format(events_table.name))
            transaction.execute(("DROP TABLE {}".format(events_table.name),))
            transaction.create_table(events_table)
            for event in events:
                transaction.execute(insert_event(host=event.host, start=event.start_time, end=event.end_time))
            logging.info("{} events inserted".format(len(events)))
        return True

    def resolve_hosts(self, connection):
        """Resolve the name of hosts."""
        tables = Database.TABLES
        get_unresolved = (StatementBuilder()
                          .select('id', 'address', 'ipversion', 'name')
                          .fromTable(tables.hosts)
                          .where(Or(IsNull('name'), EqualTo('name', '')))
                          .get())
        with connection.transaction() as transaction:
            unresolved_hosts = transaction.execute(get_unresolved()).fetchall()
        if unresolved_hosts:
            updated_hosts = {}
            for row in unresolved_hosts:
                host_id, address, ipversion, name = row
                name = gethostname(address)
                if name:
                    logging.info('host name of {} is {}'.format(address, name))
                    updated_hosts[host_id] = name
            if updated_hosts:
                update_host = (StatementBuilder()
                               .update(tables.hosts)
                               .setColumn('name', Text('name'))
                               .where(EqualTo('id', Integer('id')))
                               .get())
                with connection.transaction() as transaction:
                    for host_id in updated_hosts:
                        transaction.execute(update_host(name=updated_hosts[host_id], id=host_id))
                return True
            return False

    def upgrade_v4_to_v5(self, connection):
        """Upgrade from version 4 without table 'hosts' to version 5."""
        host_ids = {}
        tables = Database.TABLES
        with connection.transaction() as transaction:
            transaction.create_table(tables.hosts)
        upgraded = False
        for events_table in [ tables.blocklist, tables.history ]:
            if self.migrate_events_v4_to_v5(connection, host_ids, tables.hosts, events_table):
                upgraded = True
        # resolve hosts
        if self.resolve_hosts(connection):
            upgraded = True
        return upgraded

    def run(self):
        if self.exists:
            with Connection(self.filename) as connection:
                return self.upgrade_v4_to_v5(connection)
        return False
