# Copyright (C) 2018 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

__all__ = [ 'new_instance', 'upgrade_database' ]

import io
import logging

from .html import HtmlStatusPage
from .netfilter import IpTables, NfTables
from .network import *
from .sqlite import Database, DatabaseUpgrade

class CoreException(Exception):
    pass

def parse_delay(delay):
    """Return a delay in seconds. Default unit is minutes."""
    units = { 's': 1, 'm': 60, 'h': 3600, 'd': 24 * 3600, 'w': 24 * 3600 * 7 }
    if delay:
        last_character = delay[-1]
        if last_character in '0123456789':
            unit = units['m']
        elif last_character in units:
            unit = units[last_character]
            delay = delay[0:-1]
        else:
            raise CoreException('{}: invalid delay'.format(delay))
        return int(delay) * unit
    return 0

class DirectedHosts:

    """Set of hosts with direction."""

    def __init__(self):
        self.hosts = {}

    def __update_host(self, address, direction):
        if address in self.hosts:
            host = self.hosts[address]
            if host.direction != direction:
                host.direction = Host.DIRECTION_BOTH
            return True
        return False

    def __add_new_host(self, host):
        self.hosts[host.address] = host

    def add_host(self, host):
        """Add a host. If it exists, record the new direction."""
        if not self.__update_host(host.address, host.direction):
            self.__add_new_host(host)

    def add_address(self, address, direction, ipversion=None):
        """Add a host by address. If it exists, record the new direction."""
        if not self.__update_host(address, direction):
            host = Host(address, ipversion or Host.guess_ip_version(address), direction=direction)
            self.__add_new_host(host)

    def items(self):
        return [ host for host in self.hosts.values() ]


class BlockList(object):

    """Manage the list of blocked host"""

    class Status:

        """Store:
        - currently blocked hosts,
        - host in database but not in netfilter,
        - host in netfilter but not in database,
        - previously blocked hosts,
        """

        __slots__ = 'banhost', 'missing', 'orphans', 'history'

        def __init__(self, banhost=None, missing=None, orphans=None, history=None):
            self.banhost = banhost or []
            self.missing = missing or []
            self.orphans = orphans or []
            self.history = history or []

        def _asdict(self):
            return { 'banhost': self.banhost, 'missing': self.missing, 'orphans': self.orphans, 'history': self.history }

    def __init__(self, dsn, tables, quarantine, history_delay=0, compact_on_exit=False):
        super(BlockList, self).__init__()
        self.database = Database(dsn)
        self.tables = tables
        self.quarantine = quarantine
        self.history_delay = history_delay
        self.compact_on_exit = compact_on_exit
        self.trusted = {}
        self.myself = [ host.address for host in Host.resolve_myself() ]

    def close(self):
        if self.compact_on_exit:
            self.database.vacuum()
        self.database.close()

    def add_to_trusted(self, addr):
        """Add ADDR to the list of IP address that can't be blocked"""
        self.trusted[addr] = True

    def dump_banhost(self):
        """Return the content of the banhost as list and dictionary."""
        status = self.Status()
        in_addresses = [ host.address for host in self.tables.list_chain_in() ]
        out_addresses = [ host.address for host in self.tables.list_chain_out() ]
        banhost_events = self.database.iterate_current_events()
        all_addresses = []
        missing = DirectedHosts()
        for event in banhost_events:
            logging.info("{}: blocked at {} until {}".format(event.host, event.local_start_time(), event.local_end_time()))
            host = event.host.extend(start=event.start_time, end=event.end_time)
            all_addresses.append(host.address)
            status.banhost.append(host)
            for chain_name, chain_addresses, direction in [ (self.tables.chain_in, in_addresses, Host.DIRECTION_INCOMING),
                                                            (self.tables.chain_out, out_addresses, Host.DIRECTION_OUTGOING) ]:
                if not event.host.address in chain_addresses:
                    logging.error('{}: not found in chain {}'.format(event.host, chain_name))
                    missing.add_host(event.host.extend(direction=direction))
        status.missing = missing.items()

        orphans = DirectedHosts()
        for chain_name, chain_addresses, direction in [ (self.tables.chain_in, in_addresses, Host.DIRECTION_INCOMING),
                                                        (self.tables.chain_out, out_addresses, Host.DIRECTION_OUTGOING) ]:
            for address in chain_addresses:
                if not address in all_addresses:
                    logging.error('{}: blocked host in chain {} not in database'.format(address, chain_name))
                    orphans.add_address(address, direction)
        status.orphans = orphans.items()
        return status

    def dump_history(self):
        """Return the content of the history as list and dictionary."""
        result = []
        timelines = TimeLines.generate(self.database.iterate_past_events())
        for timeline in timelines:
            first = timeline[0]
            last = timeline[-1]
            host_id = str(first.host)
            count = len(timeline)
            logging.info("{}: {} times between {} and {}".format(first.host, count, first.local_start_time(), last.local_end_time()))
            host = first.host.extend(count=count, start=first.start_time, end=last.end_time)
            result.append(host)
        return result

    def status(self):
        """Log the status of the blocked hosts."""
        status = self.dump_banhost()
        status.history = self.dump_history()
        return status

    def update(self):
        """Remove hosts that have served their sentence."""
        now = Event.now()
        self.database.delete_past_events_older_than(now - self.history_delay)
        events = self.database.delete_expired_events(now, keep=self.history_delay > 0)
        if events:
            logging.info("updating list at {}".format(format_time(now)))
            state = self.tables.get_current_state()
            for event in events:
                self.tables.delete(event.host, state=state)
        self.database.delete_hosts_without_events()

    def rebuild(self):
        """Rebuild the database from the current tables rules."""
        logging.info("rebuilding database")
        banhost_events = {}
        for event in self.database.iterate_current_events():
            banhost_events[event.host.address] = event
        self.database.clear_current_events()
        inserted_hosts = {}
        for list_chain in [ self.tables.list_chain_in, self.tables.list_chain_out ]:
            for host in list_chain():
                if not host.address in inserted_hosts:
                    if host.address in banhost_events:
                        # Keep the attributes of known events
                        event = banhost_events[host.address]
                    else:
                        event = Event.make(host, duration=self.quarantine)
                    self.database.insert_new_event(event)
                    inserted_hosts[host.address] = host

    def add_host(self, host, start_time=None):
        """Add a new host to the banhost."""
        if not host.address in self.myself and not host.address in self.trusted:
            known_host = self.database.get_host(host.address)
            if known_host:
                host = known_host
            else:
                host.resolve()
            count = self.database.count_past_events_for(host.address)
            event = Event.make(host, start_time, self.quarantine * (count + 1))
            inserted_event = self.database.insert_new_event(event)
            if event != inserted_event:
                logging.warning('{}: already blocked at {}'.format(event.host, inserted_event.local_start_time()))
            else:
                logging.info('banhosting {}'.format(host))
                self.tables.append(host)

    def add(self, name_or_addr):
        """Add all hosts matching a name or address."""
        for host in Host.resolve_hosts(name_or_addr):
            self.add_host(host)

    def remove(self, address):
        """Remove a host from the banhost."""
        event = self.database.delete_current_event(address)
        logging.info("removing {} (blocked at {})".format(event.host, event.local_start_time()))
        self.tables.delete(event.host)

    def restore(self):
        """Restore the tables rules from the database."""
        logging.info("restoring rules")
        status = self.dump_banhost()
        if status.missing:
            self.tables.bulk_append(status.missing)
        if status.orphans:
            self.tables.bulk_delete(status.orphans)

    def generate_html(self, filename):
        """Generate an HTML file from the database content."""
        banhost_events = self.database.collect_current_events()
        history_events = self.database.collect_past_events()
        with io.open(filename, 'w') as outfh:
            HtmlStatusPage(outfh).generate(banhost_events, history_events)

def new_instance(config, no_act=False):
    """Create an instance of BlockList from configuration"""
    if not config.has_option('files', 'database'):
        raise CoreException('database not specified')

    database = config.get('files', 'database')

    quarantine = parse_delay(config.get('policy', 'expire')) # seconds

    history_delay = 0
    if config.has_option('history', 'expire'):
        history_delay = parse_delay(config.get('history', 'expire')) # seconds

    tables_config = config['tables']
    chain_in = target_in = chain_out = target_out = None
    if 'chain_in' in tables_config:
        chain_in = tables_config['chain_in']
    if 'target_in' in tables_config:
        target_in = tables_config['target_in']
    if 'chain_out' in tables_config:
        chain_out = tables_config['chain_out']
    if 'target_out' in tables_config:
        target_out = tables_config['target_out']

    if not 'framework' in tables_config:
        raise CoreException('framework not set (nftables or iptables)')

    framework_name = tables_config['framework']
    if framework_name == 'iptables':
        tables = IpTables(chain_in, target_in, chain_out, target_out, no_act)
    elif framework_name == 'nftables':
        family = tables_config.get('family', 'inet')
        table_name = tables_config['table']
        tables = NfTables(family, table_name, chain_in, target_in, chain_out, target_out, no_act)
    else:
        raise CoreException('{}: invalid framework'.format(framework_name))

    framework_config = config[framework_name] if config.has_section(framework_name) else {}

    if 'command' in framework_config:
        import shlex
        tables.set_command(shlex.split(framework_config['command'].strip()))
    elif framework_name == 'iptables': # For iptables only
        if 'path4' in framework_config:
            tables.set_command([ framework_config['path4'] ], ipversion=4)
        if 'path6' in framework_config:
            tables.set_command([ framework_config['path6'] ], ipversion=6)

    compact_on_exit = False
    if config.has_option('files', 'compact_on_exit'):
        compact_on_exit = config.getboolean('files', 'compact_on_exit')

    blocklist = BlockList(database, tables, quarantine, history_delay=history_delay, compact_on_exit=compact_on_exit)

    if config.has_option('policy', 'trusted'):
        for addr in config.get('policy', 'trusted').split():
            logging.debug('{}: added to trusted'.format(addr))
            blocklist.add_to_trusted(addr)

    return blocklist

def upgrade_database(config):
    """Upgrade the database schema if needed."""
    database = config.get('files', 'database')
    upgrader = DatabaseUpgrade(database)
    if upgrader.run():
        logging.info("database schema has been upgraded")
    else:
        logging.info("database schema is up to date")
