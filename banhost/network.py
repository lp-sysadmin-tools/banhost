# Copyright (C) 2005-2016 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

__all__ = [ 'Host', 'Event', 'TimeLines', 'format_time', 'format_duration' ]

import logging
import re
import socket
import time

from copy import copy

def gethostname(addr):
    """Return host name."""
    try:
        return socket.gethostbyaddr(addr)[0]
    except socket.herror:
        pass
    return None

def format_time(timestamp):
    """Format a string in local time"""
    return time.strftime("%Y/%m/%d %X", time.localtime(timestamp))

def format_duration(seconds):
    units = [ 'hour', 'minute', 'second' ]
    durations = []
    while units:
        rest = seconds % 60
        unit = units.pop()
        if rest > 1:
            unit = '{}s'.format(unit)
        if rest > 0:
            durations.insert(0, (rest, unit))
        seconds = int(seconds / 60)
    return ', '.join([ '{} {}'.format(*tuple) for tuple in durations ])

class Host:

    """A host with either a IPv4 or IPv6 address and a start time."""

    ipv4_re = re.compile(r'\d{1,3}(?:\.\d{1,3}){3}$')
    ipv6_re = re.compile(r'(?:[0-9a-fA-F]{,4})(?::[0-9a-fA-F]{,4}){1,}$')

    DIRECTION_BOTH, DIRECTION_INCOMING, DIRECTION_OUTGOING = range(3)

    def __init__(self, address, ipversion, name=None, direction=DIRECTION_BOTH):
        self.address = address
        self.ipversion = ipversion
        self.name = name
        self.direction = direction

    def resolve(self):
        """Resolve the host name."""
        if self.name == None:
            self.name = gethostname(self.address) or ''
            if not self.name:
                logging.info("{}: address can't be resolved".format(self.address))

    def _asdict(self):
        """Return the minimal attributes as a dictionary."""
        return { key: value for key, value in self.__dict__.items()
                 if value and key != 'ipversion' }

    def __str__(self):
        if self.name:
            return "{} ({})".format(self.address, self.name)
        return self.address

    def __repr__(self):
        return str((self.address, self.ipversion))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.address == other.address and self.ipversion == other.ipversion

    def __lt__(self, other):
        return self.ipversion < other.ipversion or self.address < other.address

    def extend(self, **attrs):
        """Copy the current object and add the attributes."""
        host = copy(self)
        for name, value in attrs.items():
            setattr(host, name, value)
        return host

    @staticmethod
    def guess_ip_version(address):
        if Host.ipv6_re.match(address):
            return 6
        elif Host.ipv4_re.match(address):
            return 4
        return 0

    @staticmethod
    def resolve_hosts(ip_or_name):
        """Return the list of host IP corresponding to the IP or name."""
        ip_version = Host.guess_ip_version(ip_or_name)
        if ip_version > 0:
            host = Host(ip_or_name, ip_version)
            host.resolve()
            return [ host ]
        hosts = []
        for family, proto, stype, cname, sockaddr in socket.getaddrinfo(ip_or_name, None, type=socket.SOCK_DGRAM):
            if family == socket.AF_INET:
                hosts.append(Host(sockaddr[0], 4, ip_or_name))
            elif family == socket.AF_INET6:
                hosts.append(Host(sockaddr[0], 6, ip_or_name))
        return hosts

    @staticmethod
    def resolve_myself():
        """Return the list of IP addresses for this host."""
        return Host.resolve_hosts(socket.gethostname())


class Event:

    """Event on a host with a start time and end time."""

    def __init__(self, host, start_time, end_time):
        self.host = host
        self.start_time = start_time
        self.end_time = end_time

    def local_start_time(self):
        if not self.start_time:
            return "<undefined>"
        return format_time(self.start_time)

    def local_end_time(self):
        if not self.end_time:
            return "<undefined>"
        return format_time(self.end_time)

    def duration(self):
        return self.end_time - self.start_time

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.host == other.host and self.start_time == other.start_time and self.end_time == other.end_time

    def __lt__(self, other):
        return self.start_time < other.start_time or self.end_time < other.end_time or self.host < other.host

    def __str__(self):
        return '{} from {} to {}'.format(self.host, format_time(self.start_time), format_time(self.end_time))

    def __repr__(self):
        return str((self.host, self.start_time, self.end_time))

    @staticmethod
    def now():
        return int(time.time())

    @staticmethod
    def make(host, start_time=None, duration=None):
        start_time = start_time or Event.now()
        if not duration:
            raise Exception('undefined duration in event')
        return Event(host, start_time, start_time + duration)


class TimeLines:

    """The sequence of events by hosts."""

    def __init__(self):
        self.events = {}

    def add_event(self, event):
        timeline = self.events.setdefault(event.host.address, [])
        timeline.append(event)

    def finalize(self):
        result = []
        for timeline in self.events.values():
            timeline.sort(key=lambda event: event.start_time)
            result.append(timeline)
        result.sort(key=lambda x: x[0].start_time)
        return result

    @staticmethod
    def generate(events):
        timelines = TimeLines()
        for event in events:
            timelines.add_event(event)
        return timelines.finalize()
