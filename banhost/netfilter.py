# Copyright (C) 2005-2019 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import json
import logging

from .network import Host
from .process import Call, Capture, JsonProcess, JsonProcessError, is_executable

class InvalidJsonError(Exception):
    pass

def environment_variables(names):
    """Environment restricted to the given names"""
    import os
    env = {}
    for key in names:
        if key in os.environ:
            env[key] = os.environ[key]
    return env

def few_items_string(items, max=2):
    """String representing at most the MAX first items."""
    if len(items) > max:
        items = items[0:max] + [ '...' ]
    return ', '.join([ str(item) for item in items ])

class NetfilterTables:

    """Base class for netfilter iptables or nftables."""

    EXPORTED_ENV_VARS = [ 'PATH', 'USER', 'LOGNAME', 'LANG' ]

    def __init__(self, chain_in, target_in, chain_out, target_out, no_act):
        super().__init__()
        self.chain_in = chain_in
        self.target_in = target_in
        self.chain_out = chain_out
        self.target_out = target_out
        self.no_act = no_act
        self.child_env = environment_variables(self.EXPORTED_ENV_VARS)

    def bulk_append(self, items):
        """Append hosts to the input and/or output chain."""
        for host in items:
            self.append(host)

    def bulk_delete(self, items, state=None):
        """Delete hosts from the input and/or output chain."""
        for host in items:
            self.delete(host, state=state)

    def get_current_state(self, direction=Host.DIRECTION_BOTH):
        """Return the internal state of the firewall for future use."""
        return None

class IpTables(NetfilterTables):

    """IpTables front-end"""

    DEFAULT_COMMANDS = { 4: [ '/sbin/iptables' ], 6: [ '/sbin/ip6tables' ]}

    class HostMatcher:

        """Match lines returned by iptables.

        INCOMING   all  --  198.51.100.69        0.0.0.0/0
        OUTGOING   all  --  0.0.0.0/0            198.51.100.69

        INCOMING  all      fc00:4f8:0:2::69  ::/0
        OUTGOING  all      ::/0                 fc00:4f8:0:2::69
        """

        def __init__(self, target, is_source, ipversion=None):
            self.hosts = []
            self.target = target
            self.address_index = -2 if is_source else -1
            self.ipversion = ipversion

        def __call__(self, line):
            tokens = line.split()
            if tokens and tokens[0] == self.target:
                host = Host(tokens[self.address_index], self.ipversion)
                self.hosts.append(host)

    def __init__(self, chain_in, target_in, chain_out, target_out, no_act):
        super().__init__(chain_in, target_in, chain_out, target_out, no_act)
        self.commands = dict(self.DEFAULT_COMMANDS)
        self.chains = [ None, self.chain_in, self.chain_out ]
        self.targets = [ None, self.target_in, self.target_out ]
        self.selections = [ None, '--source', '--destination' ]

    def __execute_towards(self, direction, action, host, check=True):
        command = self.commands[host.ipversion]
        cmdline = command + [ action, self.chains[direction],
                              self.selections[direction], host.address,
                              '--jump', self.targets[direction] ]
        logging.debug("executing {}".format(' '.join(cmdline)))
        if not self.no_act:
            retcode = Call(self.child_env)(cmdline, check=check)
            return retcode == 0

    def __execute(self, action, host, check=True):
        status = True
        if host.direction != Host.DIRECTION_OUTGOING: # incoming or both
            if not self.__execute_towards(Host.DIRECTION_INCOMING, action, host, check=check):
                status = False
        if host.direction != Host.DIRECTION_INCOMING: # outgoing or both
            if not self.__execute_towards(Host.DIRECTION_OUTGOING, action, host, check=check):
                status = False
        return status

    def append(self, host):
        """Append an host to the input and output chain."""
        self.__execute('--append', host)

    def delete(self, host, state=None):
        """Delete an host from the input and output chain."""
        return self.__execute('--delete', host, check=False)

    def __list(self, chain, target, is_source):
        collect = self.HostMatcher(target, is_source)
        for ipversion in self.commands:
            collect.ipversion = ipversion
            command = self.commands[ipversion]
            Capture(self.child_env, collect)(command + [ '--numeric', '--list', chain ])
        return collect.hosts

    def list_chain_in(self):
        """List of hosts in input chain."""
        return self.__list(self.chain_in, self.target_in, True)

    def list_chain_out(self):
        """List of hosts in output chain."""
        return self.__list(self.chain_out, self.target_out, False)

    def set_command(self, command, ipversion=4):
        """Path of IPv4 command."""
        self.commands[ipversion] = command

    def is_available(self):
        for command in self.commands.values():
            if not is_executable(command):
                return False
        return True


class NftJsonResponse:

    """Decode responses sent in JSON format by nft"""

    ROOT_ENTRY = "nftables"
    METAINFO_ENTRY = "metainfo"
    VERSION_ENTRY = "json_schema_version"
    CHAIN_ENTRY = "chain"
    RULE_ENTRY = "rule"

    SUPPORTED_VERSION = 1

    IP_VERSIONS = { 'ip': 4, 'ip6': 6 }

    def __init__(self, response):
        try:
            items = response[self.ROOT_ENTRY]
            metainfo = items.pop(0)[self.METAINFO_ENTRY]
            version = metainfo[self.VERSION_ENTRY]
            self.chains = []
            self.rules = []
            if version > self.SUPPORTED_VERSION:
                raise InvalidJsonError("nftable version {} not supported".format(version))
            for item in items:
                kind = [ key for key in item.keys() ][0]
                if kind == self.CHAIN_ENTRY:
                    self.chains.append(item[kind])
                elif kind == self.RULE_ENTRY:
                    self.rules.append(item[kind])
                else:
                    raise InvalidJsonError("invalid item: {}".format(json.dumps(item)))
        except KeyError as ex:
            raise InvalidJsonError("missing entry: {}".format(json.dumps(response))) from ex
        except IndexError as ex:
            raise InvalidJsonError("missing item: {}".format(json.dumps(response))) from ex

    def get_hosts(self, address_tag):
        hosts = []
        for rule in self.rules:
            for expr in rule.get("expr", []):
                field = expr.get("match", {}).get("left", {}).get("payload", {}).get("field")
                if field == address_tag:
                    protocol = expr.get("match", {}).get("left", {}).get("payload", {}).get("protocol")
                    address = expr.get("match", {}).get("right")
                    if address:
                        ipversion = self.IP_VERSIONS.get(protocol)
                        hosts.append(Host(address, ipversion))
        return hosts

    def list_handles(self, address):
        """List handles of rules matching an address

        Returns a list of tuples (chain, handle)."""
        handles = []
        for rule in self.rules:
            try:
                chain = rule['chain']
            except KeyError:
                logging.error('rule has no chain: {}'.format(json.dumps(rule)))
                continue
            for expr in rule.get("expr", []):
                matched_address = expr.get("match", {}).get("right")
                if matched_address == address:
                    handles.append((chain, rule["handle"]))
        return handles


class NfTables(NetfilterTables):

    """NfTables front-end"""

    DEFAULT_COMMAND = [ '/usr/sbin/nft' ]

    NFT_ARGS = [ '--file', '-', '--json' ]

    PROTOCOLS = { 4: 'ip', 6: 'ip6' }

    def __init__(self, family, table, chain_in, target_in, chain_out, target_out, no_act):
        super().__init__(chain_in, target_in, chain_out, target_out, no_act)
        self.family = family
        self.table = table
        self.nft = JsonProcess(self.DEFAULT_COMMAND + self.NFT_ARGS, self.child_env)

    def __append_command(self, commands, chain, target, payload_field, ipversion, address):
        commands.append(
            { "add":
              { "rule": { "family": self.family, "table": self.table, "chain": chain,
                          "expr": [ { "match": { "op": "==",
                                                 "left": { "payload": { "protocol": ipversion,
                                                                        "field": payload_field } },
                                                 "right": address } },
                                    { "jump": { "target": target } } ] } } })

    def __bulk_error(self, hosts, msg):
        """Log an error for a list of hosts."""
        logging.error('{}: {}'.format(few_items_string(hosts), msg))

    def bulk_append(self, items):
        """Append hosts to the input and/or output chain."""
        commands = []
        for host in items:
            ipversion = self.PROTOCOLS[host.ipversion]
            if host.direction != Host.DIRECTION_OUTGOING: # incoming or both
                self.__append_command(commands, self.chain_in, self.target_in, 'saddr', ipversion, host.address)
            if host.direction != Host.DIRECTION_INCOMING: # outgoing or both
                self.__append_command(commands, self.chain_out, self.target_out, 'daddr', ipversion, host.address)
        request = { "nftables": commands }
        self.nft.send(request, self.no_act)

    def append(self, host):
        """Append an host to the input and/or output chain."""
        self.bulk_append([ host ])

    def get_current_state(self, direction=Host.DIRECTION_BOTH):
        """Return the internal state of the firewall for future use."""
        commands = []
        if direction != Host.DIRECTION_OUTGOING: # incoming or both
            commands.append(
                { "list": { "chain": {"family": self.family, "table": self.table, "name": self.chain_in } } })
        if direction != Host.DIRECTION_INCOMING: # outgoing or both
            commands.append(
                { "list": { "chain": {"family": self.family, "table": self.table, "name": self.chain_out } } })
        request = { "nftables": commands }
        responses = [ NftJsonResponse(obj) for obj in self.nft.send(request) ]
        return responses

    def __delete_command(self, commands, chain, handle):
        commands.append({ "delete":
                          {"rule":
                           {"family": self.family, "table": "filter",
                            "chain": chain, "handle": handle } } })

    def bulk_delete(self, items, state=None):
        """Delete hosts from the input and/or output chain."""
        if not state:
            state = self.get_current_state()

        commands = []
        for host in items:
            handles = { self.chain_in: [], self.chain_out: [] }
            handle_count = 0
            for resp in state:
                for chain, handle in resp.list_handles(host.address):
                    handles[chain].append(handle)
                    handle_count += 1
            if handle_count == 0:
                logging.error('{}: no rule matching host'.format(host))
            else:
                for excluded, chain in [ (Host.DIRECTION_OUTGOING, self.chain_in),
                                         (Host.DIRECTION_INCOMING, self.chain_out) ]:
                    if host.direction != excluded and chain in handles:
                        for handle in handles[chain]:
                            self.__delete_command(commands, chain, handle)

        if not commands:
            self.__bulk_error(items, 'nothing to delete')
        else:
            request = { "nftables": commands }
            try:
                self.nft.send(request, self.no_act)
                return True
            except JsonProcessError as ex:
                self.__bulk_error(items, 'deletion failed')
        return False

    def delete(self, host, state=None):
        """Delete an host from the input and/or output chain."""
        self.bulk_delete([ host ], state=state)

    def list_chain(self, chain, address_tag):
        request = { "nftables": [
            { "list": { "chain": {"family": self.family, "table": self.table, "name": chain } } }
        ] }
        objects = self.nft.send(request)
        if not objects:
            return []
        response = NftJsonResponse(objects[0])
        return response.get_hosts(address_tag)

    def list_chain_in(self):
        """List of hosts in input chain."""
        return self.list_chain(self.chain_in, 'saddr')

    def list_chain_out(self):
        """List of hosts in output chain."""
        return self.list_chain(self.chain_out, 'daddr')

    def set_command(self, command):
        """Nft command line."""
        self.nft.set_args(command + self.NFT_ARGS)

    def is_available(self):
        return self.nft.is_executable()
