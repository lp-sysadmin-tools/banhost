# Copyright (C) 2018 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io

from collections import namedtuple

__all__ = [ 'Integer', 'Text', 'List',
            'And', 'Or', 'Union',
            'EqualTo', 'LessThan', 'LessThanOrEqualTo', 'GreaterThan', 'GreaterThanOrEqualTo',
            'IsIn', 'IsNotIn', 'IsNotNull', 'IsNull', 'Like',
            'Table', 'SQLError', 'StatementBuilder' ]

class SQLError(Exception):
    pass

Litteral = namedtuple('Litteral', ('value'))
Integer = namedtuple('Integer', ('name'))
Text = namedtuple('Text', ('name'))

class Operand:

    """Value potentialy enclosed in parenthesis."""

    def __init__(self, parens=False):
        self.parens = parens

class List(Operand):

    def __init__(self, name):
        super().__init__(True)
        self.name = name

class Operator(Operand):

    FORMAT = ' {} '

    def __init__(self, *operands, parens=False):
        super().__init__(parens)
        self.name = self.NAME
        self.operands = list(operands)

    def sign(self):
        return self.FORMAT.format(self.name)

class CommaOperator(Operator):
    NAME = ','
    FORMAT = '{} '

class UnaryOperator(Operator):

    FORMAT = ' {}'

    def __init__(self, value):
        super().__init__(value)

class BinaryOperator(Operator):

    def __init__(self, lvalue, rvalue):
        super().__init__(lvalue, rvalue)

class ConditionalOperator(Operator):

    def __init__(self, *operands):
        super().__init__(*operands)
        for operand in self.operands:
            if isinstance(operand, ConditionalOperator) and operand.name != self.name:
                operand.parens = True

class And(ConditionalOperator):
    NAME = 'AND'

class Or(ConditionalOperator):
    NAME = 'OR'

class Union(Operator):
    NAME = 'UNION'

class EqualTo(BinaryOperator):
    NAME = '='

class NotEqualTo(BinaryOperator):
    NAME = '!='

class LessThan(BinaryOperator):
    NAME = '<'

class LessThanOrEqualTo(BinaryOperator):
    NAME = '<='

class GreaterThan(BinaryOperator):
    NAME = '>'

class GreaterThanOrEqualTo(BinaryOperator):
    NAME = '>='

class IsIn(BinaryOperator):

    NAME = 'IN'

    def __init__(self, lvalue, rvalue):
        if isinstance(rvalue, list):
            rvalue = CommaOperator(*tuple(rvalue), parens=True)
        elif isinstance(rvalue, List):
            pass
        elif isinstance(rvalue, Operand):
            rvalue.parens = True
        else:
            raise SQLError('{}: operator expects a list as R-value'.format(self.NAME))
        super().__init__(lvalue, rvalue)

class IsNotIn(IsIn):
    NAME = 'NOT IN'

class Like(BinaryOperator):
    NAME = 'LIKE'

class IsNull(UnaryOperator):
    NAME = 'IS NULL'

class IsNotNull(UnaryOperator):
    NAME = 'IS NOT NULL'

OrderBy = namedtuple('OrderBy', ('column', 'direction'))

class Statement(Operand):

    """SQL Statement with placeholders."""

    def __init__(self, sql, parameters, context=None):
        self.sql = sql
        self.parameters = parameters or []
        self.context = context or sql

    def __call__(self, **variables):
        """Generate the statement using the variables."""
        if not self.parameters:
            return (self.sql,)
        parameters = []
        kw = {}
        for param in self.parameters:
            if isinstance(param, Litteral):
                parameters.append(param.value)
            elif isinstance(param, List):
                values = variables[param.name]
                kw[param.name] = ', '.join(['?'] * len(values))
                parameters = parameters + values
            else:
                parameters.append(variables[param.name])
        sql = self.sql
        if kw:
            sql = self.sql.format(**kw)
        return (sql, tuple(parameters))

    def __str__(self):
        return '{} with {}'.format(self.sql, self.parameters)

class Table:

    """Interface to SQLite table."""

    def __init__(self, name, columns, primary=None, unique=[]):
        self.name = name
        self.columns = columns
        self.column_types = {}
        for name, ctype in self.columns:
            self.column_types[name] = ctype
        self.primary = primary
        self.unique = unique

    def hasColumn(self, column):
        return column in self.column_types

    def firstIsAutoIncrement(self):
        """First column is an auto increment"""
        return self.primary and self.columns and self.columns[0] == (self.primary, 'integer')

    def schema(self):
        """SQL statement to create the table if it doesn't exist."""
        columns = []
        for name, coltype in self.columns:
            if name == self.primary:
                coltype = '{} PRIMARY KEY'.format(coltype)
            elif name in self.unique:
                coltype = '{} UNIQUE'.format(coltype)
            columns.append('{} {}'.format(name, coltype))
        return Statement("CREATE TABLE IF NOT EXISTS {} ({})".format(self.name, ', '.join(columns)), None)()

class StatementBuilder:

    """Build a statement step by step and return it with method get()."""

    def __init__(self):
        self.action = None
        self.direction = None
        self.columns = []
        self.tables = []
        self.aliases = {}
        self.safe_values = set() # Non litterals
        self.parameters = []
        self.condition = None
        self.orders = []
        self.assignments = []

    def __addTable(self, table, alias, direction):
        self.direction = direction
        self.tables.append(table)
        columns = [ cname for cname, ctype in table.columns ]
        for cname in columns:
            self.safe_values.add(cname)
            self.safe_values.add('{}.{}'.format(table.name, cname))
        self.aliases[table.name] = alias
        if alias:
            for cname in columns:
                self.safe_values.add('{}.{}'.format(alias, cname))
        return self

    def __setAction(self, action):
        if self.action:
            raise SQLError('{}: action already set'.format(self.action))
        self.action = action

    def quoteValue(self, value):
        """Guess if a value is a litteral."""
        if (isinstance(value, str) or isinstance(value, int)) and not value in self.safe_values:
            return Litteral(value)
        return value

    def insertInto(self, table):
        """Insert rows"""
        self.__setAction('INSERT')
        return self.__addTable(table, None, 'INTO')

    def delete(self):
        """Delete rows."""
        self.__setAction('DELETE')
        return self

    def select(self, *columns):
        """Select columns."""
        self.__setAction('SELECT')
        self.columns = list(columns)
        return self

    def count(self, column='*'):
        """Count rows."""
        self.__setAction('COUNT')
        self.columns = [ column ]
        return self

    def update(self, table):
        """Update table."""
        self.__setAction('UPDATE')
        return self.__addTable(table, None, None)

    def fromTable(self, table, alias=None):
        if not self.action:
            raise SQLError('no action set')
        return self.__addTable(table, alias, 'FROM')

    def inColumns(self, *columns):
        """Insert columns."""
        if not self.action:
            raise SQLError('no action set')
        elif self.action != 'INSERT':
            raise SQLError('{}: cannot insert columns'.format(self.action))
        elif self.columns:
            raise SQLError('{}: columns already set'.format(self.action))
        self.columns = list(columns)
        return self

    def setColumn(self, column, value):
        """Add an assignment."""
        self.assignments.append((column, self.quoteValue(value)))
        return self

    def values(self, *litterals):
        """Set values to insert."""
        if self.direction != 'INTO':
            raise SQLError('{}: invalid action with values'.format(self.action))
        self.parameters = tuple([ self.quoteValue(value) for value in litterals ])
        return self

    def where(self, condition):
        """Set the where-condition."""
        if self.direction == 'INTO':
            raise SQLError('{}: conditions are not supported'.format(self.action))
        elif not isinstance(condition, Operator):
            raise SQLError('invalid condition type')
        self.condition = condition
        return self

    def orderBy(self, column):
        """Default direction order."""
        self.orders.append(OrderBy(column, None))
        return self

    def ascendentOrderBy(self, column):
        """Ascendent order."""
        self.orders.append(OrderBy(column, 'ASC'))
        return self

    def decendentOrderBy(self, column):
        """Descendent order."""
        self.orders.append(OrderBy(column, 'DESC'))
        return self

    def validate(self):
        """Validate the statement."""
        if not self.action:
            raise SQLError('no action set')
        for column in self.columns:
            # Check if column belongs to one of the tables
            if not column in self.safe_values and column != '*' and not (self.action == 'COUNT' and column == '*'):
                raise SQLError('{}: invalid column'.format(column))
            # Check if column without table prefix is not ambiguous
            if not '.' in column:
                owner = None
                for table in self.tables:
                    if table.hasColumn(column):
                        if not owner:
                            owner = table.name
                        else:
                            raise SQLError('{}: ambiguous column name belongs to {} and {}'.format(column, owner, table.name))

    def __collectCondition(self, sql_stream, parameters, condition, level):
        try:
            separator = ''
            operator = condition.sign()
            for operand in condition.operands:
                sql_stream.write(separator)
                enclosed = hasattr(operand, 'parens') and operand.parens
                if enclosed:
                    sql_stream.write('(')
                if isinstance(operand, Litteral) or isinstance(operand, Integer) or isinstance(operand, Text):
                    sql_stream.write('?')
                    parameters.append(operand)
                elif isinstance(operand, int) or (isinstance(operand, str) and not operand in self.safe_values):
                    sql_stream.write('?')
                    parameters.append(Litteral(operand))
                elif isinstance(operand, List):
                    # insert a placeholder {name} that will be replaced by the correct number of sign '?'
                    sql_stream.write('{{{}}}'.format(operand.name))
                    parameters.append(operand)
                elif isinstance(operand, Statement):
                    sql_stream.write(operand.sql)
                    for param in operand.parameters:
                        parameters.append(param)
                elif isinstance(operand, Operator):
                    self.__collectCondition(sql_stream, parameters, operand, level + 1)
                else:
                    sql_stream.write(str(operand))
                separator = operator
                if enclosed:
                    sql_stream.write(')')
            if isinstance(condition, UnaryOperator):
                sql_stream.write(operator)
        finally:
            pass

    def __collectWhere(self, sql_stream, previous_parameters=None):
        """Write the where-clause and return the parameters."""
        parameters = previous_parameters or []
        if self.condition:
            sql_stream.write(' WHERE ')
            self.__collectCondition(sql_stream, parameters, self.condition, 0)
        return parameters

    def __collectAssignments(self, sql_stream, previous_parameters=None):
        """Write the where-clause and return the parameters."""
        parameters = previous_parameters or []
        if self.assignments:
            sql_stream.write(' SET')
            separator = ' '
            for column, value in self.assignments:
                sql_stream.write('{}{} = '.format(separator, column))
                if isinstance(value, Litteral) or isinstance(value, Integer) or isinstance(value, Text):
                    sql_stream.write('?')
                    parameters.append(value)
                else:
                    sql_stream.write(value)
                separator = ', '
        return parameters

    def __getTables(self, sql_stream):
        separator = ''
        for table in self.tables:
            sql_stream.write('{} {}'.format(separator, table.name))
            separator = ','
            alias = self.aliases[table.name]
            if alias:
                sql_stream.write(' AS {}'.format(alias))

    def get(self):
        """Return the statement."""
        self.validate()
        sql_stream = io.StringIO()
        is_count = (self.action == 'COUNT')
        sql_stream.write('SELECT' if is_count else self.action)
        if self.direction == 'FROM':
            if self.columns:
                if is_count:
                    sql_stream.write(' COUNT({})'.format(self.columns[0]))
                else:
                    sql_stream.write(' {}'.format(', '.join(self.columns)))
            sql_stream.write(' {}'.format(self.direction))
            self.__getTables(sql_stream)
            parameters = self.__collectWhere(sql_stream)
            if self.orders:
                sql_stream.write(' ORDER BY')
                separator = ' '
                for order in self.orders:
                    sql_stream.write(separator)
                    sql_stream.write(str(order.column))
                    if order.direction:
                        sql_stream.write(' {}'.format(order.direction.upper()))
                    separator = ', '
        elif self.direction == 'INTO':
            sql_stream.write(' {}'.format(self.direction))
            self.__getTables(sql_stream)
            table = self.tables[0]
            columns = table.columns
            if self.columns:
                columns = self.columns
                sql_stream.write(' ({})'.format(', '.join(columns)))
            values = []
            if len(columns) > len(self.parameters) and table.firstIsAutoIncrement():
                values = [ 'NULL' ]
            if self.parameters:
                parameters = self.parameters
                sql_stream.write(' VALUES ({})'.format(', '.join(values + ['?'] * len(parameters))))
        elif self.direction == None:
            self.__getTables(sql_stream)
            parameters = self.__collectAssignments(sql_stream)
            parameters = self.__collectWhere(sql_stream, previous_parameters=parameters)
        sql = sql_stream.getvalue()
        context = '{} {} {}'.format(self.action, self.direction, ', '.join([ table.name for table in self.tables ]))
        return Statement(sql, parameters, context)
