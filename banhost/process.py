# Copyright (C) 2005-2019 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io
import json
import logging
import os
import subprocess

def is_executable(command):
    """Check if command exists and is executable"""
    return os.access(command, os.X_OK)

class Call:

    """Execute a command."""

    def __init__(self, child_env):
        self.child_env = child_env

    def __call__(self, args, check=True):
        """Run the command. Raise an exception if exit code is not 0 unless check is False."""
        ret = subprocess.run(args, env=self.child_env, check=check)
        return ret.returncode

class Capture:

    """Execute a command. Pass lines to the filter."""

    def __init__(self, child_env, filter_line):
        self.child_env = child_env
        self.filter_line = filter_line

    def __call__(self, args):
        filter_line = self.filter_line
        proc = subprocess.Popen(args, stdout=subprocess.PIPE, env=self.child_env)
        try:
            for line in proc.stdout:
                filter_line(line.decode('UTF-8').rstrip())
        finally:
            retcode = proc.wait()
        if retcode != 0:
            raise subprocess.CalledProcessError(retcode, args)

class JsonProcessError(Exception):

    """Exception on execution error"""

    def __init__(self, retcode, input_data):
        self.retcode = retcode
        self.input_data = input_data

class JsonProcess:

    """Execute a command that takes JSON as input and print JSON on output."""

    def __init__(self, args, child_env):
        self.args = args
        self.child_env = child_env

    def set_args(self, args):
        self.args = args

    def is_executable(self):
        return is_executable(self.args[0])

    def communicate(self, input_data):
        """Execute process, pass input and return output."""
        logging.debug("sending to nft: {}".format(input_data))
        proc = subprocess.Popen(self.args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, env=self.child_env, universal_newlines=True)
        output_data = None
        try:
            output_data, errors = proc.communicate(input=input_data + "\n")
        finally:
            retcode = proc.wait()
        if retcode != 0:
            raise JsonProcessError(retcode, input_data)
        logging.debug("receiving from nft: {}".format(output_data))
        return output_data

    def send(self, input_object, dry_run=False):
        """Send INPUT_OBJECT as JSON and read JSON answers."""
        input_data = json.dumps(input_object)
        if dry_run:
            logging.info('executing: {} with {}'.format(' '.join(self.args), input_data))
            return []
        output_data = self.communicate(input_data)
        result = None
        if output_data:
            with io.StringIO(output_data) as infh:
                result = [ json.loads(line) for line in infh ]
        return result
