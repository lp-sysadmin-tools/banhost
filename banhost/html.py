# Copyright (C) 2005-2016 Laurent Pelecq <lpelecq+banhost@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import socket
import time

from collections import namedtuple

from .network import TimeLines, format_time, format_duration

class Translate:

    """Translate number to a new origin and scale"""

    def __init__(self, origin, factor):
        self.origin = origin
        self.factor = factor

    def move(self, value):
        return value - self.origin

    def scale(self, value):
        return value * self.factor

    def __call__(self, value):
        return self.scale(self.move(value))


class IndentedOutput:

    """Output with indentation"""

    def __init__(self, outfh):
        self.outfh = outfh
        self.level = 0
        self.bol = True # beginning of line

    def indent(self):
        self.level += 1

    def unindent(self):
        self.level -= 1

    def write(self, text):
        if self.bol:
            self.outfh.write('  ' * self.level)
            self.bol = False
        self.outfh.write(text)

    def newline(self):
        self.write('\n')
        self.bol = True

    def writelines(self, lines):
        for line in lines:
            self.write(line)
            self.newline()

    def writenl(self, *lines):
        self.writelines(lines)


class Tag:

    """Print an HTML tag"""

    def __init__(self, output, name, inline, attrs):
        self.output = output
        self.name = name
        self.inline = inline
        self.attrs = attrs

    def open(self):
        self.output.write('<{}'.format(self.name))
        for name in self.attrs:
            self.output.write(' {}="{}"'.format(name, self.attrs[name]))
        self.output.write('>')
        if not self.inline:
            self.output.newline()
            self.output.indent()

    def close(self):
        if not self.inline:
            self.output.unindent()
        self.output.writenl('</{}>'.format(self.name))

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, extype, exvalue, traceback):
        self.close()

class Printer(IndentedOutput):

    """HTML printer"""

    def __init__(self, outfh):
        super().__init__(outfh)
        self.writenl('<!DOCTYPE html>')

    def tag(self, name_, **attrs):
        return Tag(self, name_, False, attrs)

    def inlinetag(self, name_, text, **attrs):
        with Tag(self, name_, True, attrs):
            self.write(text)

    def singletag(self, name_, **attrs):
        Tag(self, name_, True, attrs).open()
        self.newline()

TimeRange = namedtuple('TimeRange', ('min', 'max'))
Unit = namedtuple('Unit', ('name', 'seconds'))

class HtmlStatusPage:

    WIDTH = 800
    FONT_SIZE = 12
    MAX_CANVAS_HEIGHT = 32767
    HEADER_HEIGHT = 22
    LINEBOX_HEIGHT = 10
    SEPARATOR_HEIGHT= 4
    ROW_HEIGHT = FONT_SIZE + LINEBOX_HEIGHT + SEPARATOR_HEIGHT

    COLOR_BACKGROUND = "WhiteSmoke"
    COLOR_HOSTNAME = "DarkGreen"
    COLOR_PAST = "Orchid"
    COLOR_CURRENT = "Purple"
    COLOR_SEPARATOR = "Gainsboro"

    CSS = """body {{
      background-color: {bg};
      margin: 10px;
    }}
    table {{
      border-collapse:collapse;
    }}
    th, td {{
      border:1px solid black;
    }}
    """.format(bg=COLOR_BACKGROUND)

    SCRIPT = """function drawLine(ctx, x1, y1, x2, y2, color, size) {{
      if (color) ctx.strokeStyle = color;
      if (size) ctx.lineWidth = size;
      ctx.beginPath();
      ctx.moveTo(x1, y1);
      ctx.lineTo(x2, y2);
      ctx.closePath();
      ctx.stroke();
    }}
    function drawChart(chart_id, unit_name, unit_width, timelines) {{
      var c = document.getElementById(chart_id);
      var ctx = c.getContext("2d");
      var total_width = {width};
      var header_height = {header};
      var font_size = {fontsize};
      ctx.font = ctx.font.replace(/\d+px/, font_size + "px");
      ctx.textAlign = "start";
      ctx.textBaseline = "top";
      var row_height = {rowheight};
      var small_gap = {sepheight};
      // unit rule
      ctx.fillStyle = "black";
      var line_min_y = font_size + small_gap/2;
      var line_max_y = header_height - small_gap;
      drawLine(ctx, 0, line_max_y, total_width, line_max_y, "black", small_gap/2);
      ctx.textAlign = "start";
      var unit_x, unit_val;
      for (unit_x = 0, unit_val = 0; unit_x < total_width; unit_x+=unit_width, unit_val++) {{
         drawLine(ctx, unit_x, line_min_y, unit_x, line_max_y);
         ctx.fillText(unit_val, unit_x, 0);
         ctx.textAlign = "center";
      }}
      ctx.textAlign = "end";
      ctx.fillText("(" + unit_name + ")", total_width, 0);
      // events
      ctx.textAlign = "start";
      ctx.fillStyle = "{hostcolor}";
      timelines.forEach(function(tline, index) {{
        var top = header_height + index*row_height;
        var name = tline.name;
        var name_width = ctx.measureText(name).width;
        var name_x = tline.events[0].start;
        if (name_x + name_width > total_width)
           name_x = total_width - name_width;
        ctx.fillText(name, name_x, top);
        var y = top + font_size + (row_height - font_size) / 3;
        tline.events.forEach(function(evt) {{
          drawLine(ctx, evt.start, y, evt.end, y, evt.current ? "{currentcolor}" : "{pastcolor}", font_size / 4);
        }});
        y = top + row_height - small_gap;
        drawLine(ctx, 0, y, total_width, y, "{sepcolor}", 1);
      }});
    }};
    """.format(width=WIDTH, header=HEADER_HEIGHT, fontsize=FONT_SIZE, sepheight=SEPARATOR_HEIGHT, rowheight=ROW_HEIGHT, hostcolor=COLOR_HOSTNAME, pastcolor=COLOR_PAST, currentcolor=COLOR_CURRENT, sepcolor=COLOR_SEPARATOR)

    units = [ ('weeks', 24 * 3600 * 7), ('days', 24 * 3600), ('hours', 3600), ('minutes', 60), ('seconds', 1) ]

    def __init__(self, outfh):
        self.printer = Printer(outfh)
        self.hostname = socket.getfqdn()
        self.now = format_time(time.time())

    def timeline_table(self, timelines, summary):
        tag = self.printer.tag
        inline = self.printer.inlinetag
        with tag('table', summary=summary):
            with tag('tr'):
                inline('th', 'IP address')
                inline('th', 'hostname')
                inline('th', 'start time')
                inline('th', 'duration')
            for tline in timelines:
                first = True
                for event in tline:
                    with tag('tr'):
                        if first:
                            first = False
                            inline('td', event.host.address, rowspan=len(tline))
                            inline('td', event.host.name or '', rowspan=len(tline))
                        inline('td', event.local_start_time())
                        inline('td', format_duration(event.duration()))

    def generate_table(self, title, summary, events):
        """Generate a table of events."""
        inline = self.printer.inlinetag
        timelines = TimeLines.generate(events)
        if timelines:
            inline('h2', title)
            self.timeline_table(timelines, summary)

    def generate_chart(self, chart_index, tr, unit, timelines):
        tag = self.printer.tag
        inline = self.printer.inlinetag
        height = len(timelines) * self.ROW_HEIGHT + self.HEADER_HEIGHT
        chart_id = 'chart{}'.format(chart_index)
        with tag('canvas', id=chart_id, width=self.WIDTH, height=height):
            pass
        write = self.printer.write
        writenl = self.printer.writenl
        with tag('script', type="text/javascript"):
            fit_to_page = Translate(tr.min, self.WIDTH / (tr.max - tr.min))
            unit_width = fit_to_page.scale(unit.seconds)
            writenl("drawChart('{}', '{}', {}, [".format(chart_id, unit.name, unit_width))
            for tline in timelines:
                write("""  {{ name: "{}", events: [""".format(tline[0].host))
                jsevents = []
                for event in tline:
                    start = fit_to_page(event.start_time)
                    end = fit_to_page(event.end_time)
                    is_current = 'true' if event.is_current else 'false'
                    jsevents.append("{{ start: {}, end: {}, current: {} }}".format(start, end, is_current))
                write(', '.join(jsevents))
                writenl("""] },""")
            writenl("]);")

    def generate_charts(self, timelines):
        max_lines = int((self.MAX_CANVAS_HEIGHT - self.HEADER_HEIGHT) / self.ROW_HEIGHT)
        chart_count = int(len(timelines) / max_lines) + 1
        batch_size = int(len(timelines) / chart_count)
        start_index = 0
        end_index = batch_size
        min_time = min([ tline[0].start_time for tline in timelines ])
        max_time = max([ tline[-1].end_time for tline in timelines ])
        period = max_time - min_time
        for unit_name, unit_seconds in self.units:
            if period > unit_seconds:
                break
        tr = TimeRange(min_time, max_time)
        for chart_index in range(chart_count):
            self.generate_chart(chart_index, tr, Unit(unit_name, unit_seconds), timelines[start_index:end_index])
            start_index = end_index
            end_index += batch_size

    def generate(self, banhost, history):
        tag = self.printer.tag
        inline = self.printer.inlinetag
        single = self.printer.singletag
        title = 'Banned hosts on {}'.format(self.hostname)
        with tag('html', lang='en'):
            with tag('head'):
                single('meta', charset='utf-8')
                single('meta', name='author', content='banhost')
                single('meta', name='description', content='Banhost command status')
                single('link', rel='icon', href='icon.png', type='image/png', sizes='32x32')
                single('link', rel='license', href='http://creativecommons.org/licenses/by-sa/2.0/en/')
                with tag('script'):
                    self.printer.writelines(self.SCRIPT.split("\n    "))
                with tag('style'):
                    self.printer.writelines(self.CSS.split("\n    "))
                inline('title', title)
            with tag('body'):
                inline('h1', title)
                inline('p', 'Current time: {}'.format(self.now))
                if banhost or history:
                    timelines = TimeLines()
                    for event in history:
                        event.is_current = False
                        timelines.add_event(event)
                    for event in banhost:
                        event.is_current = True
                        timelines.add_event(event)
                    self.generate_charts(timelines.finalize())
                self.generate_table('Banned', "Hosts currently blocked", banhost)
                self.generate_table('History', "Hosts no longer blocked", history)
